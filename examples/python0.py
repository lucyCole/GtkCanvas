import gi
gi.require_versions(
	{
		"GtkCanvas": "1.0",
		"Gtk": "4.0",
		"Adw": "1",
	}
)
from gi.repository import (
	Gtk,
	Gsk,
	GObject,
	Gdk,
	Adw,
	GLib,
	Gio,
	GtkCanvas,
	Graphene,
)
from math import (
	sin,
)

def generateTestWidget():
	movedNode= Gtk.Box.new(
		Gtk.Orientation.HORIZONTAL,
		4
	)

	bx2= Gtk.Box.new(
		Gtk.Orientation.HORIZONTAL,
		4
	)
	bn0= Adw.Bin.new()
	bn2= Adw.Bin.new()
	bx3= Gtk.Box.new(
		Gtk.Orientation.VERTICAL,
		4
	)
	bt2= Gtk.Button.new_with_label( "Minyon")
	def onClick( button):
		containing= bt2
		while True:
			if isinstance( containing, GtkCanvas.NodeWrapper):
				wrapper= containing
				spinny= Gtk.Button.new_with_label( "'~'")
				spinny.set_sensitive( False)
				spinny.add_css_class( "destructive-action")
				spinny.add_css_class( "circular")
				placement= GtkCanvas.SurroundingWidgetPlacement()
				edges= [
					GtkCanvas.SurroundingWidgetEdge.LEFT,
					GtkCanvas.SurroundingWidgetEdge.TOP,
					GtkCanvas.SurroundingWidgetEdge.RIGHT,
					GtkCanvas.SurroundingWidgetEdge.BOTTOM,
				]
				currentEdgeIndex= -1
				def nextEdge():
					nonlocal currentEdgeIndex
					currentEdgeIndex+= 1
					if currentEdgeIndex== len( edges):
						currentEdgeIndex= 0
					return edges[ currentEdgeIndex]
				placement.locationScaling= GtkCanvas.PlacementScaling.RELATIVE_TO_WHOLE
				placement.offsetScaling= GtkCanvas.PlacementScaling.PIXELS
				edge= nextEdge()
				offset= 0.0
				location= 0.0
				placement.edge= edge
				placement.offset= offset
				placement.location= location
				wrapper.setSurroundingWidget(
					spinny,
					placement,
					False
				)
				increment= 0.01
				def onTick( w, fc):
					nonlocal location
					# ( double) fc.get_frame_time()
					location+= increment
					if location> 1.0:
						location-= 1.0
						# Next edge
						edge= nextEdge()
						if edge== GtkCanvas.SurroundingWidgetEdge.RIGHT:
							spinny.remove_css_class( "circular")
						else:
							spinny.add_css_class( "circular")
						placement.edge= edge
					# print( "LOCATION: %f\n", location)
					if edge== GtkCanvas.SurroundingWidgetEdge.BOTTOM:
						placement.offset= sin( location* 44.0)* 10.0
					placement.location= location
					wrapper.setSurroundingWidget(
						spinny,
						placement,
						True
					)
					return GLib.Source.CONTINUE
				containing.add_tick_callback( onTick)
				break

			containing= containing.get_parent()
			if containing== None:
				break
	
	bt2.connect(
		"clicked",
		onClick
	)
	bt2.add_css_class( "opaque")
	bx3.append( bt2)
	bt3= Gtk.Button.new_with_label( "32wnx")
	bt3.add_css_class( "opaque")
	bx3.append( bt3)
	bn3= Adw.Bin.new()
	dA2= GtkCanvas.DragWidget.new()
	dA2.set_size_request( 100, 20)
	# dA2.movedNode= movedNode
	bn3.set_child( dA2)
	bx3.append( bn3)
	bn2.set_child( bx3)
	bn0.set_child( bn2)
	bx2.append( bn0)
	bn1= Adw.Bin.new()
	bt1= Gtk.Button.new_with_label( "cnhjej non-blocking")
	def onClick( button):
		print( "cnhjej CLICKED\n")
	bt1.connect( "clicked", onClick)
	# Gtk.GestureClick gc1= ( Gtk.GestureClick) bt1.observe_controllers()[ 1]
	# gc1.begin.connect( ( n)=> { print( "( BUTTON CLICK) BEGIN\n")})
	# gc1.cancel.connect( ( n)=> { print( "( BUTTON CLICK) CANCEL\n")})
	# gc1.end.connect( ( n)=> { print( "( BUTTON CLICK) END\n")})
	# gc1.sequence_state_changed.connect( ( n, s)=> { print( "( BUTTON CLICK) SEQUENCE STATE CHANGED: %s\n", s.to_string())})
	# gc1.update.connect( ( n)=> { print( "( BUTTON CLICK) UPDATE\n")})
	bt1.add_css_class( "opaque")
	bn1.set_child( bt1)
	bx2.append( bn1)
	movedNode.append( bx2)

	dA0= GtkCanvas.DragWidget.new()
	# dA0.movedNode= movedNode
	bx1= Gtk.Box.new(
		Gtk.Orientation.VERTICAL,
		4
	)
	dA1= GtkCanvas.DragWidget.new()
	# dA1.movedNode= movedNode
	bx1.append( dA1)
	bt0= Gtk.Button.new_with_label( "vcdhjb blocking")
	bt0.add_css_class( "opaque")
	gc= bt0.observe_controllers()[ 1]
	def q( n, x, y):
		print( "vcdhjb CLICKED")
		gc.set_state( Gtk.EventSequenceState.CLAIMED)
	gc.connect(
		"pressed",
		q,
	)
	bx1.append( bt0)
	dA0.contained= bx1
	movedNode.append( dA0)

	return movedNode

def run( app):
	win= Gtk.ApplicationWindow.new( app)

	default= Gdk.Display.get_default();
	provider= Gtk.CssProvider.new();
	css= """
*:selected {
	border-style: solid;
	border-color: red;
	border-width: 5px;
}
*:hover {
	outline-style: solid;
	outline-color: yellow;
	outline-width: 2px;
}
""";
	css= """
NodeWrapper {
	outline-style: solid;
	outline-width: 3px;
	outline-color: @borders;
	outline-offset: -3px;
	border-radius: 8px;
}
NodeWrapper > Bin {
	outline-style: solid;
	outline-width: 3px;
	outline-color: @borders;
	border-radius: 4px;
}
NodeWrapper:selected > Bin {
	outline-color: darkred;
}
NodeWrapper.inSelectionBound {
	outline-color: lightblue;
}
NodeWrapper.additionCandidate > Bin {
	outline-color: pink;
}
NodeWrapper.removalCandidate > Bin {
	outline-color: lightgreen;
}
CanvasSelection {
	border-radius: 8px;
	outline-color: blue;
	outline-style: solid;
	outline-width: 3px;
	outline-offset: -3px;
}
ConnectionEndStub {
	border-radius: 999999px;
	background-color: darkgrey;
	outline-style: solid;
	outline-width: 4px;
	outline-color: lightgrey;
}
ConnectionEndStub:hover {
	outline-color: lightblue;
}
ConnectionEndStub.connectionTargetHover {
	outline-color: yellow;
}
ConnectionEndStub.partialAccept {
	background-color: lightblue;
}
ConnectionEndStub.fullAccept {
	background-color: pink;
}
ConnectionEndStub.noAccept {
	background-color: lightgreen;
}
ConnectionEndStub.selfConnectProposed {
	background-color: green;
}
Connection {
	background: lightgrey;
}
Connection.accepted {
	background: blue;
}
Connection.connectionTargetHover.accepted {
	background: pink;
}
Connection.connectionTargetHover.rejected {
	background: lightgreen;
}
Connection.connectionTargetHover.selfConnectProposed {
	background: green;
}

Canvas > Connection.group0 {
	
}
Canvas > Connection.group0:selected {
	
}
Canvas > Connection.group0:hover {
	
}
/* Can use background snapshot order as in gtk demo maze example to support css theming of connection-s*/
"""
	provider.load_from_string( css)
	Gtk.StyleContext.add_provider_for_display(
		default,
		provider,
		Gtk.STYLE_PROVIDER_PRIORITY_USER,
	)

	win= Gtk.ApplicationWindow.new( app)
	# print( GtkCanvas)
	# print( dir( GtkCanvas))
	# print( GtkCanvas.CanvasView)
	# print( dir( GtkCanvas.CanvasView))
	# q= GObject.GType.from_name( "GtkCanvasCanvasView")
	# print( q)
	canvasView= GtkCanvas.CanvasView.new()
	# print( dir( canvasView))
	from pprint import pprint

	canvas= canvasView.get_canvas()

	baseTransform= Gsk.Transform.new()
	testWidget= generateTestWidget()
	point= Graphene.Point()
	point.x= 20
	point.y= 40
	baseTransform= baseTransform.translate( point)
	testWidgetNode= canvas.addNode( testWidget, baseTransform)
	testWidgetOut0= GtkCanvas.ConnectionEndStub.new()
	testWidgetOut1= GtkCanvas.ConnectionEndStub.new()
	def getSurroundingWidgetPlacement(
		edge,
		location,
		offset
	):
		placement= GtkCanvas.SurroundingWidgetPlacement()
		placement.edge= edge
		placement.location= location
		placement.offset= offset
		return placement


	# for some reason the bool argument here is not detected in vala
	# maybe debug with check-ing arg-(.)
	testWidgetNode.setSurroundingWidget(
		testWidgetOut0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.BOTTOM,
			location= 0.1,
			offset= 0.0
		),
		False
	)
	testWidgetNode.setSurroundingWidget(
		testWidgetOut1,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.BOTTOM,
			location= 0.7,
			offset= 0.0
		),
		False
	)



	button1= Gtk.Button.new_with_label( "HIya as well")
	button1.add_css_class( "opaque")
	button1.connect( "clicked", lambda _: print( "CLICKED\n"))
	button1Node= canvas.addNode( button1)
	button1In0= GtkCanvas.ConnectionEndStub.new()
	button1Node.setSurroundingWidget(
		button1In0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.LEFT,
			location= 0.5,
			offset= 0.0
		),
		False
	)
	button2= Gtk.Button.new_with_label( "njefkdhsbj")
	button2.add_css_class( "opaque")
	button2Node= canvas.addNode( button2)
	button2In0= GtkCanvas.ConnectionEndStub.new()
	button2In1= GtkCanvas.ConnectionEndStub.new()

	button2Node.setSurroundingWidget(
		button2In0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.TOP,
			location= 0.5,
			offset= 0.0
		),
		False
	)
	button2Node.setSurroundingWidget(
		button2In1,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.RIGHT,
			location= 0.2,
			offset= 0
		),
		False
	)
	canvas.formConnection(
		button2In0,
		testWidgetOut0
	)
	canvas.formConnection(
		button2In1,
		button2In0
	)
	canvas.formConnection(
		button2In1,
		button1In0
	)


	entry= Gtk.Entry.new()
	entryNode= canvas.addNode(
		entry,
		baseTransform
	)
	entryStub= GtkCanvas.ConnectionEndStub.new()
	entryNode.setSurroundingWidget(
		entryStub,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.TOP,
			location= 0.5,
			offset= 0.0
		),
		False
	)
	canvas.formConnection(
		entryStub,
		testWidgetOut1
	)

	layoutAll= Gtk.Box.new(
		Gtk.Orientation.HORIZONTAL,
		3
	)
	layoutAllDrag= Gtk.Button.new()
	layoutAllDrag.add_css_class( "opaque")
	layoutAll.append( layoutAllDrag)
	layoutAllButton= Gtk.Button.new_with_label( "layout all")
	layoutAllButton.add_css_class( "opaque")
	layoutAll.append( layoutAllButton)
	layoutAllNode= canvas.addNode( layoutAll)
	layoutAllStub0= GtkCanvas.ConnectionEndStub.new()
	layoutAllStub1= GtkCanvas.ConnectionEndStub.new()
	layoutAllNode.setSurroundingWidget(
		layoutAllStub0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.TOP,
			location= 0.9,
			offset= 0.0
		),
		False
	)
	layoutAllNode.setSurroundingWidget(
		layoutAllStub1,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.LEFT,
			location= 0.5,
			offset= 0
		),
		False
	)
	canvas.formConnection(
		layoutAllStub1,
		entryStub
	)


	layoutModels= [
		"DOT",
		"NEATO",
		"FDP",
		"SFDP",
		"CIRCO",
		"TWOPI",
		"OSAGE",
		"PATCHWORK"
	]
	layoutSelect= Gtk.DropDown.new_from_strings( layoutModels)
	layoutSelect.add_css_class( "opaque")
	layoutSelectNode= canvas.addNode( layoutSelect)
	layoutSelectStub0= GtkCanvas.ConnectionEndStub.new()
	layoutSelectStub1= GtkCanvas.ConnectionEndStub.new()
	layoutSelectStub2= GtkCanvas.ConnectionEndStub.new()
	layoutSelectNode.setSurroundingWidget(
		layoutSelectStub0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.BOTTOM,
			location= 0.9,
			offset= 0.0
		),
		False
	)
	layoutSelectNode.setSurroundingWidget(
		layoutSelectStub1,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.BOTTOM,
			location= 0.5,
			offset= 0
		),
		False
	)
	layoutSelectNode.setSurroundingWidget(
		layoutSelectStub2,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.RIGHT,
			location= 0.8,
			offset= 0
		),
		False
	)
	canvas.formConnection(
		layoutSelectStub1,
		entryStub
	)
	canvas.formConnection(
		layoutSelectStub2,
		layoutAllStub1
	)
	canvas.formConnection(
		layoutSelectStub2,
		layoutAllStub0
	)




	layoutSelectTion= Gtk.Box.new(
		Gtk.Orientation.HORIZONTAL,
		3
	)
	layoutSelectTionDrag= Gtk.Button.new()
	layoutSelectTionDrag.add_css_class( "opaque")
	layoutSelectTion.append( layoutSelectTionDrag)
	layoutSelectTionButton= Gtk.Button.new_with_label( "layout select-tion")
	layoutSelectTionButton.add_css_class( "opaque")
	layoutSelectTion.append( layoutSelectTionButton)
	layoutSelectTionNode= canvas.addNode( layoutSelectTion)
	print( "PROBLEM ID: %i\n", layoutSelectNode.count)
	layoutSelectTionStub0= GtkCanvas.ConnectionEndStub.new()
	layoutSelectTionStub1= GtkCanvas.ConnectionEndStub.new()
	layoutSelectTionStub2= GtkCanvas.ConnectionEndStub.new()
	layoutSelectTionNode.setSurroundingWidget(
		layoutSelectTionStub0,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.TOP,
			location= 0.1,
			offset= 0.0
		),
		False
	)
	layoutSelectTionNode.setSurroundingWidget(
		layoutSelectTionStub1,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.RIGHT,
			location= 0.1,
			offset= 0
		),
		False
	)
	layoutSelectTionNode.setSurroundingWidget(
		layoutSelectTionStub2,
		getSurroundingWidgetPlacement(
			edge= GtkCanvas.SurroundingWidgetEdge.BOTTOM,
			location= 0.1,
			offset= 0
		),
		False
	)
	canvas.formConnection(
		layoutSelectTionStub0,
		layoutSelectStub0
	)
	canvas.formConnection(
		layoutSelectTionStub0,
		button2In0
	)
	canvas.formConnection(
		layoutSelectTionStub1,
		button2In0
	)
	canvas.formConnection(
		layoutSelectTionStub2,
		entryStub
	)

	def onClick( b):
		# selection= Gee.HashSet< GtkCanvas.NodeWrapper>.new()
		# foreach( Gtk.Widget selected in canvas.selection) {
		# 	selection.add( ( GtkCanvas.NodeWrapper) selected)
		# }
		
		# this is lay-ing out too big
		canvas.layout(
			# selection,
			canvas.get_selection(),

			
			GtkCanvas.CanvasLayoutModel.DOT

			# this func is broke from python
			# GtkCanvas.canvas_layout_model_fromString( layoutModels[ layoutSelect.get_selected()])
		)
	layoutSelectTionButton.connect(
		"clicked",
		onClick,
	)
	# block to not grab select-tion
	gc= layoutSelectTionButton.observe_controllers()[ 1]
	def q( n, x, y, u):
		gc.set_state( Gtk.EventSequenceState.CLAIMED)
	gc.connect(
		"pressed",
		q,
	)
	
	def onClick( b):
		# all= Gee.HashSet< GtkCanvas.NodeWrapper>.new()
		# foreach( Gtk.Widget node in canvas) {
		# 	if( node is GtkCanvas.NodeWrapper) {
		# 		all.add( ( GtkCanvas.NodeWrapper) node)
		# 	}
		# }
		canvas.layout(
			# all,
			set( canvas),
			GtkCanvas.canvas_layout_model_fromString( layoutModels[ layoutSelect.get_selected()])
		)
	layoutAllButton.connect(
		"clicked",
		onClick,
	)

	win.set_child( canvasView)
	win.present()
	print( "Setup end")



def main():
	app= Adw.Application.new(
		"a.a.a",
		Gio.ApplicationFlags.DEFAULT_FLAGS
	)
	app.connect( "activate", run)
	app.run()


if __name__== "__main__":
	main()
