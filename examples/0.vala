using GtkCanvas;

Gtk.Widget generateTestWidget() {
	var movedNode= new Gtk.Box(
		Gtk.Orientation.HORIZONTAL,
		4
	);

	var bx2= new Gtk.Box(
		Gtk.Orientation.HORIZONTAL,
		4
	);
	var bn0= new Adw.Bin();
	var bn2= new Adw.Bin();
	var bx3= new Gtk.Box(
		Gtk.Orientation.VERTICAL,
		4
	);
	var bt2= new Gtk.Button.with_label( "Minyon");
	bt2.clicked.connect( ()=> {
		Gtk.Widget? containing=  ( Gtk.Widget) bt2;
		while( true) {
			if( containing is NodeWrapper) {
				NodeWrapper wrapper= ( NodeWrapper) containing;
				Gtk.Button spinny= new Gtk.Button.with_label( "'~'");
				spinny.set_sensitive( false);
				spinny.add_css_class( "destructive-action");
				spinny.add_css_class( "circular");
				SurroundingWidgetPlacement placement= SurroundingWidgetPlacement();
				Gee.LinkedList< SurroundingWidgetEdge> edges= new Gee.LinkedList< SurroundingWidgetEdge>();
				edges.add( SurroundingWidgetEdge.LEFT);
				edges.add( SurroundingWidgetEdge.TOP);
				edges.add( SurroundingWidgetEdge.RIGHT);
				edges.add( SurroundingWidgetEdge.BOTTOM);
				Gee.Iterator< SurroundingWidgetEdge> edgeIterator= edges.iterator();
				placement.locationScaling= PlacementScaling.RELATIVE_TO_WHOLE;
				placement.offsetScaling= PlacementScaling.PIXELS;
				edgeIterator.next();
				SurroundingWidgetEdge edge= edgeIterator.get();
				double offset= 0.0;
				double location= 0.0;
				placement.edge= edge;
				placement.offset= offset;
				placement.location= location;
				wrapper.setSurroundingWidget(
					spinny,
					placement,
					false
				);
				double increment= 0.01;
				containing.add_tick_callback( ( w, fc)=> {
					bool hasNextEdge;
					// ( double) fc.get_frame_time();
					location+= increment;
					if( location> 1.0) {
						location-= 1.0;
						// Next edge
						hasNextEdge= edgeIterator.next();
						if( hasNextEdge) {
							edge= edgeIterator.get();
						} else {
							edgeIterator= edges.iterator();
							edgeIterator.next();
							edge= edgeIterator.get();
						}
						if( edge== SurroundingWidgetEdge.RIGHT) {
							spinny.remove_css_class( "circular");
						} else {
							spinny.add_css_class( "circular");
						}
						placement.edge= edge;
					}
					// print( "LOCATION: %f\n", location);
					if( edge== SurroundingWidgetEdge.BOTTOM) {
						placement.offset= Math.sin( location* 44.0)* 10.0;
					}
					placement.location= location;
					wrapper.setSurroundingWidget(
						spinny,
						placement,
						true
					);
					return GLib.Source.CONTINUE;
				});
				break;
			}
			containing= containing.get_parent();
			if( containing== null) {
				break;
			}
		}
	});
	bt2.add_css_class( "opaque");
	bx3.append( bt2);
	var bt3= new Gtk.Button.with_label( "32wnx");
	bt3.add_css_class( "opaque");
	bx3.append( bt3);
	var bn3= new Adw.Bin();
	var dA2= new DragWidget();
	dA2.set_size_request( 100, 20);
	// dA2.movedNode= movedNode;
	bn3.set_child( dA2);
	bx3.append( bn3);
	bn2.set_child( bx3);
	bn0.set_child( bn2);
	bx2.append( bn0);
	var bn1= new Adw.Bin();
	var bt1= new Gtk.Button.with_label( "cnhjej non-blocking");
	bt1.clicked.connect( ()=> print( "cnhjej CLICKED\n"));
	// Gtk.GestureClick gc1= ( Gtk.GestureClick) bt1.observe_controllers().get_object( 1);
	// gc1.begin.connect( ( n)=> { print( "( BUTTON CLICK) BEGIN\n");});
	// gc1.cancel.connect( ( n)=> { print( "( BUTTON CLICK) CANCEL\n");});
	// gc1.end.connect( ( n)=> { print( "( BUTTON CLICK) END\n");});
	// gc1.sequence_state_changed.connect( ( n, s)=> { print( "( BUTTON CLICK) SEQUENCE STATE CHANGED: %s\n", s.to_string());});
	// gc1.update.connect( ( n)=> { print( "( BUTTON CLICK) UPDATE\n");});
	bt1.add_css_class( "opaque");
	bn1.set_child( bt1);
	bx2.append( bn1);
	movedNode.append( bx2);

	var dA0= new DragWidget();
	// dA0.movedNode= movedNode;
	var bx1= new Gtk.Box(
		Gtk.Orientation.VERTICAL,
		4
	);
	var dA1= new DragWidget();
	// dA1.movedNode= movedNode;
	bx1.append( dA1);
	var bt0= new Gtk.Button.with_label( "vcdhjb blocking");
	bt0.add_css_class( "opaque");
	Gtk.GestureClick gc= ( Gtk.GestureClick) bt0.observe_controllers().get_object( 1);
	gc.pressed.connect( ( n, x, y)=> {
		print( "vcdhjb CLICKED\n");
		gc.set_state( Gtk.EventSequenceState.CLAIMED);
	});
	bx1.append( bt0);
	dA0.contained= bx1;
	movedNode.append( dA0);

	
	return movedNode;
}

void run(
	GLib.Application app
) {
	Gdk.Display default= Gdk.Display.get_default();
	Gtk.CssProvider provider= new Gtk.CssProvider();
	string css= """
*:selected {
	border-style: solid;
	border-color: red;
	border-width: 5px;
}
*:hover {
	outline-style: solid;
	outline-color: yellow;
	outline-width: 2px;
}
""";
	css= """
NodeWrapper {
	outline-style: solid;
	outline-width: 3px;
	outline-color: @borders;
	outline-offset: -3px;
	border-radius: 8px;
}
NodeWrapper > Bin {
	outline-style: solid;
	outline-width: 3px;
	outline-color: @borders;
	border-radius: 4px;
}
NodeWrapper:selected > Bin {
	outline-color: darkred;
}
NodeWrapper.inSelectionBound {
	outline-color: lightblue;
}
NodeWrapper.additionCandidate > Bin {
	outline-color: pink;
}
NodeWrapper.removalCandidate > Bin {
	outline-color: lightgreen;
}
CanvasSelection {
	border-radius: 8px;
	outline-color: blue;
	outline-style: solid;
	outline-width: 3px;
	outline-offset: -3px;
}
ConnectionEndStub {
	border-radius: 999999px;
	background-color: darkgrey;
	outline-style: solid;
	outline-width: 4px;
	outline-color: lightgrey;
}
ConnectionEndStub:hover {
	outline-color: lightblue;
}
ConnectionEndStub.connectionTargetHover {
	outline-color: yellow;
}
ConnectionEndStub.partialAccept {
	background-color: lightblue;
}
ConnectionEndStub.fullAccept {
	background-color: pink;
}
ConnectionEndStub.noAccept {
	background-color: lightgreen;
}
ConnectionEndStub.selfConnectProposed {
	background-color: green;
}
Connection {
	background: lightgrey;
}
Connection.accepted {
	background: blue;
}
Connection.connectionTargetHover.accepted {
	background: pink;
}
Connection.connectionTargetHover.rejected {
	background: lightgreen;
}
Connection.connectionTargetHover.selfConnectProposed {
	background: green;
}

Canvas > Connection.group0 {
	
}
Canvas > Connection.group0:selected {
	
}
Canvas > Connection.group0:hover {
	
}
/* Can use background snapshot order as in gtk demo maze example to support css theming of connection-s*/
""";
	provider.load_from_string( css);
	Gtk.StyleContext.add_provider_for_display(
		default,
		provider,
		Gtk.STYLE_PROVIDER_PRIORITY_USER
	);

	print( "%i\n", Gtk.StateFlags.VISITED| Gtk.StateFlags.FOCUSED);
	print( "%i\n", Gtk.StateFlags.VISITED);
	print( "%i\n", Gtk.StateFlags.FOCUSED);
	print( "%i\n", ( Gtk.StateFlags.VISITED| Gtk.StateFlags.FOCUSED)- Gtk.StateFlags.VISITED);
	print( "%i\n", ( Gtk.StateFlags.VISITED| Gtk.StateFlags.FOCUSED)- Gtk.StateFlags.FOCUSED);
	print( "%i\n", Gtk.StateFlags.VISITED| Gtk.StateFlags.FOCUSED| Gtk.StateFlags.PRELIGHT);
	print( "%i\n", ( Gtk.StateFlags.VISITED| Gtk.StateFlags.FOCUSED| Gtk.StateFlags.PRELIGHT)- Gtk.StateFlags.PRELIGHT| Gtk.StateFlags.FOCUSED);
	print( "%i\n", Gtk.StateFlags.NORMAL);
	print( "%i\n", Gtk.StateFlags.ACTIVE);
	print( "%i\n", Gtk.StateFlags.PRELIGHT);
	Gtk.ApplicationWindow win= new Gtk.ApplicationWindow( ( Gtk.Application) app);
	CanvasView canvasView= new CanvasView();
	// Canvas canvas= new Canvas();

	Gsk.Transform baseTransform= new Gsk.Transform();
	// Gtk.Button button0= new Gtk.Button.with_label( "HIya");
	var testWidget= generateTestWidget();
	var point= Graphene.Point();
	point.x= 20;
	point.y= 40;
	baseTransform= baseTransform.translate( point);
	NodeWrapper testWidgetNode= canvasView.canvas.addNode( testWidget, baseTransform);
	ConnectionEndStub testWidgetOut0= new ConnectionEndStub();
	ConnectionEndStub testWidgetOut1= new ConnectionEndStub();
	testWidgetNode.setSurroundingWidget(
		( Gtk.Widget) testWidgetOut0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.BOTTOM,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.1,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	testWidgetNode.setSurroundingWidget(
		( Gtk.Widget) testWidgetOut1,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.BOTTOM,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.7,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);



	Gtk.Button button1= new Gtk.Button.with_label( "HIya as well");
	button1.add_css_class( "opaque");
	button1.clicked.connect( ()=> { print( "CLICKED\n");});
	NodeWrapper button1Node= canvasView.canvas.addNode( button1);
	ConnectionEndStub button1In0= new ConnectionEndStub();
	button1Node.setSurroundingWidget(
		( Gtk.Widget) button1In0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.LEFT,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.5,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	Gtk.Button button2= new Gtk.Button.with_label( "njefkdhsbj");
	button2.add_css_class( "opaque");
	NodeWrapper button2Node= canvasView.canvas.addNode( button2);
	ConnectionEndStub button2In0= new ConnectionEndStub();
	ConnectionEndStub button2In1= new ConnectionEndStub();

	button2Node.setSurroundingWidget(
		( Gtk.Widget) button2In0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.TOP,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.5,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	button2Node.setSurroundingWidget(
		( Gtk.Widget) button2In1,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.RIGHT,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.2,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	canvasView.canvas.formConnection(
		button2In0,
		testWidgetOut0
	);
	canvasView.canvas.formConnection(
		button2In1,
		button2In0
	);
	canvasView.canvas.formConnection(
		button2In1,
		button1In0
	);


	Gtk.Entry entry= new Gtk.Entry();
	NodeWrapper entryNode= canvasView.canvas.addNode(
		entry,
		baseTransform
	);
	ConnectionEndStub entryStub= new ConnectionEndStub();
	entryNode.setSurroundingWidget(
		( Gtk.Widget) entryStub,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.TOP,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.5,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	canvasView.canvas.formConnection(
		entryStub,
		testWidgetOut1
	);

	Gtk.Box layoutAll= new Gtk.Box(
		Gtk.Orientation.HORIZONTAL,
		3
	);
	Gtk.Button layoutAllDrag= new Gtk.Button();
	layoutAllDrag.add_css_class( "opaque");
	layoutAll.append( layoutAllDrag);
	Gtk.Button layoutAllButton= new Gtk.Button.with_label( "layout all");
	layoutAllButton.add_css_class( "opaque");
	layoutAll.append( layoutAllButton);
	NodeWrapper layoutAllNode= canvasView.canvas.addNode( layoutAll);
	ConnectionEndStub layoutAllStub0= new ConnectionEndStub();
	ConnectionEndStub layoutAllStub1= new ConnectionEndStub();
	layoutAllNode.setSurroundingWidget(
		( Gtk.Widget) layoutAllStub0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.TOP,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.9,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	layoutAllNode.setSurroundingWidget(
		( Gtk.Widget) layoutAllStub1,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.LEFT,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.5,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	canvasView.canvas.formConnection(
		layoutAllStub1,
		entryStub
	);


	string[] layoutModels= {
		"DOT",
		"NEATO",
		"FDP",
		"SFDP",
		"CIRCO",
		"TWOPI",
		"OSAGE",
		"PATCHWORK"
	};
	Gtk.DropDown layoutSelect= new Gtk.DropDown.from_strings( layoutModels);
	layoutSelect.add_css_class( "opaque");
	NodeWrapper layoutSelectNode= canvasView.canvas.addNode( layoutSelect);
	ConnectionEndStub layoutSelectStub0= new ConnectionEndStub();
	ConnectionEndStub layoutSelectStub1= new ConnectionEndStub();
	ConnectionEndStub layoutSelectStub2= new ConnectionEndStub();
	layoutSelectNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectStub0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.BOTTOM,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.9,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	layoutSelectNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectStub1,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.BOTTOM,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.5,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	layoutSelectNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectStub2,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.RIGHT,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.8,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	canvasView.canvas.formConnection(
		layoutSelectStub1,
		entryStub
	);
	canvasView.canvas.formConnection(
		layoutSelectStub2,
		layoutAllStub1
	);
	canvasView.canvas.formConnection(
		layoutSelectStub2,
		layoutAllStub0
	);




	Gtk.Box layoutSelectTion= new Gtk.Box(
		Gtk.Orientation.HORIZONTAL,
		3
	);
	Gtk.Button layoutSelectTionDrag= new Gtk.Button();
	layoutSelectTionDrag.add_css_class( "opaque");
	layoutSelectTion.append( layoutSelectTionDrag);
	Gtk.Button layoutSelectTionButton= new Gtk.Button.with_label( "layout select-tion");
	layoutSelectTionButton.add_css_class( "opaque");
	layoutSelectTion.append( layoutSelectTionButton);
	NodeWrapper layoutSelectTionNode= canvasView.canvas.addNode( layoutSelectTion);
	print( "PROBLEM ID: %i\n", layoutSelectNode.count);
	ConnectionEndStub layoutSelectTionStub0= new ConnectionEndStub();
	ConnectionEndStub layoutSelectTionStub1= new ConnectionEndStub();
	ConnectionEndStub layoutSelectTionStub2= new ConnectionEndStub();
	layoutSelectTionNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectTionStub0,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.TOP,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.1,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0.0
		},
		false
	);
	layoutSelectTionNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectTionStub1,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.RIGHT,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.1,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	layoutSelectTionNode.setSurroundingWidget(
		( Gtk.Widget) layoutSelectTionStub2,
		SurroundingWidgetPlacement() {
			edge= SurroundingWidgetEdge.BOTTOM,
			locationScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			location= 0.1,
			offsetScaling= PlacementScaling.RELATIVE_TO_WHOLE,
			offset= 0
		},
		false
	);
	canvasView.canvas.formConnection(
		layoutSelectTionStub0,
		layoutSelectStub0
	);
	canvasView.canvas.formConnection(
		layoutSelectTionStub0,
		button2In0
	);
	canvasView.canvas.formConnection(
		layoutSelectTionStub1,
		button2In0
	);
	canvasView.canvas.formConnection(
		layoutSelectTionStub2,
		entryStub
	);

	layoutSelectTionButton.clicked.connect( ( b)=> {
		Gee.HashSet< NodeWrapper> selection= new Gee.HashSet< NodeWrapper>();
		foreach( Gtk.Widget selected in canvasView.canvas.selection) {
			selection.add( ( NodeWrapper) selected);
		}
		canvasView.canvas.layout(
			selection,
			CanvasLayoutModel.fromString( layoutModels[ layoutSelect.get_selected()])
		);
	});
	// block to not grab select-tion
	Gtk.GestureClick gc= ( Gtk.GestureClick) layoutSelectTionButton.observe_controllers().get_object( 1);
	gc.pressed.connect( ( n, x, y)=> {
		gc.set_state( Gtk.EventSequenceState.CLAIMED);
	});
	
	layoutAllButton.clicked.connect( ( b)=> {
		Gee.HashSet< NodeWrapper> all= new Gee.HashSet< NodeWrapper>();
		foreach( Gtk.Widget node in canvasView.canvas) {
			if( node is NodeWrapper) {
				all.add( ( NodeWrapper) node);
			}
		}
		canvasView.canvas.layout(
			all,
			CanvasLayoutModel.fromString( layoutModels[ layoutSelect.get_selected()])
		);
	});

	win.set_child( canvasView);
	win.show();
}


public int main(
	string[] args
) {
	Adw.Application app= new Adw.Application(
 		"p.p.p",
		GLib.ApplicationFlags.DEFAULT_FLAGS
	);
	app.activate.connect( run);
	app.run();
	return 0;
}
