using Gvc;

int main(
	string[] args
) {
	Gvc.Context context= new Gvc.Context();
	Gvc.Graph graph= new Gvc.Graph (
		"test",
		// Gvc.Agdirected,
		Gvc.Agundirected,
		0
	);
	Gvc.Node n= graph.create_node( "n", 1);
	Gvc.Node m= graph.create_node( "m", 1);
	Gvc.Edge e= graph.create_edge(
		n,
		m,
		null,
		1
	);
	n.safe_set(
		"color",
		"red",
		""
	);
	context.layout(
		graph,
		"dot"
	);
	GLib.FileStream writeLocation= GLib.FileStream.open( "/tmp/w.dot", "w");
	context.render(
		graph,
		"dot",
		// GLib.stdout
		writeLocation
	);
	return 0;
}
