( define-module
	( GtkCanvas guix GtkCanvas)
	; ( GtkCanvas)

	#:use-module ( guix git-download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build utils)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module ( gnu packages base)
	#:use-module ( gnu packages graphviz)
	#:use-module ( gnu packages gnome)
	#:use-module ( gnu packages glib)
	#:use-module ( gnu packages gtk)
	#:use-module ( gnu packages pkg-config)

	#:use-module ( guix download)
	#:use-module ( guix utils)
	#:use-module ( srfi srfi-1)

	#:use-module ( oop goops)

	#:export (
		GtkCanvas
	)
)

( define
	local
	#t
)


( define
	( packageInfo
		package
		result
	)
	( display
		( package-name package)
	)
	( newline)
	( +
		result
		1
	)
)
( define
	res
	( lambda
		_
		( fold-packages
			packageInfo
			0
			#:select? ( lambda
				( package)
				( and
					( equal?
						( package-build-system package)
						meson-build-system
					)
					( let*
						(
							( comb
								( lambda
									(
										combiner
										init
									)
									( lambda
										( inList)
										( fold
											( lambda
												(
													element
													result
												)
												( combiner
													element
													result
												)
											)
											init
											inList
										)
									)
								)
							)
							( orComb
								( comb
									( lambda ( q w) ( or q w))
									#f
								)
							)
						)
						( orComb
							( map
								( lambda
									( input)
									; ( display "input")
									; ( newline)
									; ( write input)
									; ( newline)
									; ( newline)
									; ( write ( cdr input))
									; ( newline)
									; ( display ( class-of ( cdr input)))
									; ( newline)
									; ( newline)
									; ( write ( cadr input))
									; ( newline)
									; ( display ( class-of ( cadr input)))
									; ( newline)
									; ( newline)
									; ( newline)
									( orComb
										( map
											( lambda
												( desired)
												( and
													( package? ( cadr input))
													( string=?
														desired
														( package-name
															( cadr input)
														)
													)
												)
											)
											( list
												"vala"
												"gobject-introspection"
											)
										)
									)
								)
								( fold
									( lambda
										(
											pList
											result
										)
										( append
											pList
											result
										)
									)
									( list)
									( map
										( lambda
											( retFunc)
											( retFunc package)
										)
										( list
											package-inputs
											package-native-inputs
											package-propagated-inputs
										)
									)
								)
							)
						)
					)
				)
			)
		)
	)
)

; ( display ( res))


; some of these maybe implicit for meson build

; vala, with valadoc
; gobject-introspection
; pkg-config

; glib2
; gobject2     think this is not provided by some other thing
; gtk4
; gee0.8
; libadwaita1

; GtkWidgetAids
; c math library       glibc maybe


; ( define
; 	GtkCanvas
; 	( let
; 		( 
; 			( projectVersion "1.0")
; 			( revision "0")
; 			( commit "f1ffb0d978d5024493452656916c81531bc10a11")
; 		)
; 		( package
; 			( name "GtkCanvas")
; 			( version
; 				( git-version
; 					projectVersion
; 					revision
; 					commit
; 				)
; 			)
; 			( source
; 				( origin
; 					( method git-fetch)
; 					( uri
; 						( git-reference
; 							( commit commit)
; 							( url "https://gitlab.gnome.org/lucyCole/GtkCanvas.git")
; 							( recursive? #false)
; 						)
; 					)
; 					( sha256
; 						( base32 "0y8yma6kd6d0696phfa75h5z15d4fnypynqcwwnjvdyzcnhfmday")
; 					)
; 				)
; 			)
; 			( build-system meson-build-system)
; 			( inputs
; 				( list
; 					lucyVala
; 					pkg-config
; 					glibc
; 				)
; 			)
; 			( propagated-inputs
; 				( list
; 					lucyGI
; 					lucyGLib
; 					lucyGtk
; 					libgee
; 					libadwaita
; 				)
; 			)
; 			( synopsis "2d canvas library for gtk")
; 			( description
; 				"Place them!
; Make little friends!
; Resize them!
; Arbitrary transform on Gsk.Transform!
; Lines between them!
; Graphviz layout!
; Move them!
; Select them!"
; 			)
; 			( license license.lgpl2.1+)
; 			( home-page "https://gitlab.gnome.org/lucyCole/GtkCanvas")
; 		)
; 	)
; )
(define valaCGraph
(package
( inherit vala)
(version "0.56.17")
( arguments
	( substitute-keyword-arguments
		( package-arguments gnuGnome.vala)
		(
			( #:configure-flags flags)
			#~(list
				"CC=gcc"
				"--enable-coverage"
				"--with-cgraph"
			)
		)
	)
)
(source (origin
(method url-fetch)
(uri (string-append "mirror://gnome/sources/vala/"
(version-major+minor version) "/"
"vala-" version ".tar.xz"))
(sha256
(base32
"0spd6ill4nnfpj13qm6700yqhrgmgkcl1wbmj9hrq17h9r70q416"))))))

( define
	GtkWidgetAids
	; ( ensureCorrectVersions
		( let
			( 
				( projectVersion "1.0")
				( revision "0")
				( commit "78e70521efd01bf9f6c26181d8567bcf698f8491")
			)
			( package
				( name "GtkWidgetAids")
				( version
					( git-version
						projectVersion
						revision
						commit
					)
				)

				( source
					( if
						local
						( local-file
							"/home/lucy/Projects/GtkWidgetAids"
							#:recursive? #true
						)
						( origin
							( method git-fetch)
							( uri
								( git-reference
									( commit commit)
									( url "https://gitlab.gnome.org/lucyCole/GtkWidgetAids.git")
									( recursive? #false)
								)
							)
							( sha256
								( base32 "1hpfv2d0yjvfm9xjs24aa7bawgdwqa4dsmdxscmhd3cl9ps0wl02")
							)
						)
					)
				)


				( build-system meson-build-system)
				( inputs
					( list
						vala
					)
				)
				( propagated-inputs
					( list
						pkg-config
						gobject-introspection
						glib
						gtk
						libgee
						libadwaita
					)
				)
				( synopsis "Gtk help-er function-s used by me: ( the packager!)")
				( description
					"Contiains, bin, selection interface, some other stuff"
				)
				( license license.lgpl2.1+)
				( home-page "https://gitlab.gnome.org/lucyCole/GtkWidgetAids")
				( properties
					'(
						( hidden? . #t)
					)
				)
			)
		)
	; )
)
( define
	GtkCanvas
	( let
		( 
			( projectVersion "1.0")
			( revision "0")
			( commit "2e94cff1d50f395cf4f4d6e1fd74e1e5ffe57705")
		)
		( package
			( name "GtkCanvas")
			( version
				( git-version
					projectVersion
					revision
					commit
				)
			)

			( source
				( if
					local
					( local-file
						"/home/lucy/Projects/GtkCanvas"
						#:recursive? #t
					)
					( origin
						( method git-fetch)
						( uri
							( git-reference
								( commit commit)
								( url "https://gitlab.gnome.org/lucyCole/GtkCanvas.git")
								( recursive? #false)
							)
						)
						( sha256
							( base32 "0qjj8r6mzg3vk7xmb20sdf83yvci3q6qd1cj3hsjhrfva47ihqf9")
						)
					)
				)
			)
			( build-system meson-build-system)
			( arguments
				( list
					#:phases #~( modify-phases
						%standard-phases
						( add-after
							'unpack
							'absolute-introspection-library
							( lambda*
								(
									#:key outputs
									#:allow-other-keys
								)
								( substitute*
									"src/meson.build"
									(
										( "@PLAINNAME@" all)
										; ( "libGtkCanvas.so.0" all)
										( string-append
											#$output
											"/lib/"
											all
										)
									)
								)
								; ( error "pop")
							)
						)
					)
				)
			)
			( inputs
				( list
					vala
					glibc
				)
			)
			( propagated-inputs
				( list
					pkg-config

					gobject-introspection
					glib
					gtk

					libgee
					libadwaita

					graphviz
					GtkWidgetAids
				)
			)
			( synopsis "2d canvas library for gtk")
			( description
				"Place them!
Make little friends!
Resize them!
Arbitrary transform on Gsk.Transform!
Lines between them!
Graphviz layout!
Move them!
Select them!"
			)
			( license license.lgpl2.1+)
			( home-page "https://gitlab.gnome.org/lucyCole/GtkCanvas")
		)
	)
)

GtkCanvas
