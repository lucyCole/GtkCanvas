// DOING DRAG SELECT, NOT SHOWING, MAYBE MESS WITH QUEUE_...

// need to set size of node-(.) when create-ing
// conform everything to the correct size-ing format, so all should be input and output from graphviz in points
// need to add parm-(.) for node-(.) and edge-(.) and override in main
// need to get pos from layout and set node pos-(.), not do-ing resize now

// make sure layout arg-(.) comply with component restriction-(.) by default, by pass-ing option-al component
// conform new-ly add-ed feature-(.) to restriction-(.) on component manipulate-tion
// clean up code


// resize as a group, customisable
// incremental layout
// edit history
// incremental binding and widget create-tion can wait until incremental layout
// it would need a method of laying out that which is not some instanciate-ed NodeWrapper
// resize reset based on common 2d coord-(.) of both shape-(.), so to target pos, cursor if edge click cause-ed
// change of view focus during lay-out if canvas view pass-ed, ig can be got from canvas


using GtkWidgetAids;

namespace GtkCanvas {
	[ Flags]
	public enum Position {
		LEFT,
		TOP,
		RIGHT,
		BOTTOM,
	}
	public void calculateWidgetResize(
		Position resizeHandle,
		Gsk.Transform initialTransform,
		Gtk.Requisition initialSize,
		int movementX,
		int movementY,
		float maxTransformX,
		float maxTransformY,
		int minWidth,
		int minHeight,
		bool allowNegative,
		out Gtk.Requisition size,
		out Gsk.Transform? transform,
		out bool transforming
	) {
		// Could take temp var-s as ref-s or out to avoid re-allocate-ion
		size= Gtk.Requisition();
		transforming= false;
		float transformX= 0.0f;
		float transformY= 0.0f;
		int width= initialSize.width;
		int height= initialSize.height;
		if( Position.LEFT in resizeHandle) {
			transforming= true;
			if( allowNegative) {
				if( movementX> initialSize.width) {
					transformX= ( float) initialSize.width;
					width= movementX- initialSize.width;
				} else {
					transformX= ( float) movementX;
					width-= movementX;
				}
			} else {
				transformX= ( float) movementX;
				width-= movementX;
			}
		}
		if( Position.TOP in resizeHandle) {
			transforming= true;
			if( allowNegative) {
				if( movementY> initialSize.height) {
					transformY= ( float) initialSize.height;
					height= movementY- initialSize.height;
				} else {
					transformY= ( float) movementY;
					height-= movementY;
				}
			} else {
				transformY= ( float) movementY;
				height-= movementY;
			}
		}
		if( Position.RIGHT in resizeHandle) {
			if( allowNegative) {
				int negWidth= initialSize.width* -1;
				if( movementX< negWidth) {
					transforming= true;
					transformX= ( float) movementX- negWidth;
					width= negWidth- movementX;
				} else {
					width+= movementX;
				}
			} else {
				width+= movementX;
			}
		}
		if( Position.BOTTOM in resizeHandle) {
			if( allowNegative) {
				int negHeight= initialSize.height* -1;
				if( movementY< negHeight) {
					transforming= true;
					transformY= ( float) movementY- negHeight;
					height= negHeight- movementY;
				} else {
					height+= movementY;
				}
			} else {
				height+= movementY;
			}
		}

		if( transforming) {
			transform= initialTransform.ref();
			Graphene.Point point= Graphene.Point();
			if( !allowNegative) {
				point.x= Math.fminf( maxTransformX, transformX);
				point.y= Math.fminf( maxTransformY, transformY);
			} else {
				point.x= transformX;
				point.y= transformY;
			}
			transform= transform.translate( point);
		} else {
			transform= null;
		}
		if( !allowNegative) {
			size.width= ( width> minWidth)? width: minWidth;
			size.height= ( height> minHeight)? height: minHeight;
		} else {
			size.width= width;
			size.height= height;
		}
	}
	public enum InteractionSource {
		PROGRAMMATIC,
		GUI
	}
	static double dragThreshold= 10.0;

	public errordomain WidgetHierarchyOrganisation {
		WIDGET_NOT_CONTAINED_BY_CANVAS,
		NO_TARGET_NODE_PRESENT
	}

	public errordomain ModifierClass {
		NON_UNIQUE_MODIFIERS
	}

	public errordomain ConnectionRejection {
		NON_BOOL_VALUE,
		NOT_REJECTION_CANDIDATE
	}

	public errordomain LayoutError {
		NODE_NOT_TARGETING_THIS_CANVAS,
		NO_BOUNDING_BOX_COMPUTED,
		NO_SIZE_SET,
		NO_POS_SET,
		NODE_TRANSFORM_FAIL
	}

	// public class CanvasContainedLayoutProperties: Gtk.LayoutChild {
	// 	public Gsk.Transform transformation { get; set; default= new Gsk.Transform();}
	// }

	/**
	* The bound drawing of the select-tion
	*/
	public class CanvasSelection: Gtk.Widget {
		class construct {
			set_css_name( "CanvasSelection");
		}
		public Gsk.Transform transform= new Gsk.Transform();
		public int width= 0;
		public int height= 0;
	}
	/**
	* The 2d plane upon which node-s are stored
	*/
	public struct DebugSquape {
		float x;
		float y;
		float width;
		float height;
		Gdk.RGBA color;
		public static DebugSquape fromGrapheneRect(
			Graphene.Rect rect,
			Gdk.RGBA color
		) {
			return DebugSquape() {
				x= rect.get_x(),
				y= rect.get_y(),
				width= rect.get_width(),
				height= rect.get_height(),
				color= color
			};
		}
	}
	// Movement of a node causes connection resize
	// Drag of connection causes connection resize
	// internal struct ConnectionEndDragCreateData {
	// }
	// internal struct ConnectionEndDragDragAwayData {
	// }
	public struct ConnectionEndDragData {
		ConnectionEndDragType type;
		ConnectionEnd startConnectionEnd;
		//
		Graphene.Point cursorPosition;
		Gee.HashSet< Connection> connections;
		ConnectionEnd? hoveredTarget;
		Gee.HashSet< Connection> acceptedConnections;
		Gee.HashSet< Connection> selfConnectProposedRejection;
		Gee.HashSet< Connection> nonAcceptanceRejection;
	}
	public struct CanvasLayoutBound {
		int x;
		int y;
	}
	public enum CanvasLayoutRatio {
		FILL,
		COMPRESS,
		AUTO,
		VALUE
	}
	public enum CanvasLayoutOverlap {
		FALSE,
		TRUE,
		SCALE
	}
	public enum CanvasLayoutNeatoMode {
		NEATO,
		KK
	}
	public enum CanvasLayoutNeatoModel {
		NEATO,
		CIRCUIT,
		SUBSET
	}
	public struct CanvasLayoutArgs {
		bool directed;
		CanvasLayoutBound? maxBound;
		CanvasLayoutRatio? ratioType;
		float? ratioValue;
		CanvasLayoutOverlap? overlap;
		/**
		* dot specific
		*/
		float? dotMinimumNodeSeperation;
		/**
		* neato specific
		*/
		CanvasLayoutNeatoMode? neatoMode;
		CanvasLayoutNeatoModel? neatoModel;
		float? neatoSeed;
		float? neatoEpsilonCutoff;
		/**
		* twopi specific
		*/
		NodeWrapper? twopiRoot;
		float? twopiRankSeperation;
		/**
		* circo specific
		*/
		NodeWrapper? circoRoot;
		float? circoMinimumNodeSeperation;
		/**
		* fdp specific
		*/
		float? fdpIdealNodeSeperation;
		int? fdpMaxIterations;
		float? fdpSeed;
		public CanvasLayoutArgs.default() {
			// when graph structure supports it, and added, this can take an optional graph and use it to set the directed flag
			directed= false;
			maxBound= null;
			ratioType= null;
			ratioValue= null;
			overlap= null;
			dotMinimumNodeSeperation= null;
			neatoMode= null;
			neatoModel= null;
			neatoSeed= null;
			neatoEpsilonCutoff= null;
			twopiRoot= null;
			twopiRankSeperation= null;
			circoRoot= null;
			circoMinimumNodeSeperation= null;
			fdpIdealNodeSeperation= null;
			fdpMaxIterations= null;
			fdpSeed= null;
		}
	}
	public enum CanvasLayoutModel {
		DOT,
		NEATO,
		FDP,
		SFDP,
		CIRCO,
		TWOPI,
		OSAGE,
		PATCHWORK;

		public static CanvasLayoutModel? fromString( string s) {
			switch ( s.down()) {
				case "dot":
					return CanvasLayoutModel.DOT;
				case "neato":
					return CanvasLayoutModel.NEATO;
				case "fdp":
					return CanvasLayoutModel.FDP;
				case "sfdp":
					return CanvasLayoutModel.SFDP;
				case "circo":
					return CanvasLayoutModel.CIRCO;
				case "twopi":
					return CanvasLayoutModel.TWOPI;
				case "osage":
					return CanvasLayoutModel.OSAGE;
				case "patchwork":
					return CanvasLayoutModel.PATCHWORK;
			}
			return null;
		}
		public string get_name() {
			// EnumClass enumc= ( EnumClass) typeof ( CanvasLayoutModel).class_ref();
			// unowned EnumValue? eval= enumc.get_value( this);
			// return_val_if_fail( eval!= null, null);
			// return eval.value_nick;
			switch ( this) {
				case CanvasLayoutModel.DOT:
					return "DOT";
				case CanvasLayoutModel.NEATO:
					return "NEATO";
				case CanvasLayoutModel.FDP:
					return "FDP";
				case CanvasLayoutModel.SFDP:
					return "SFDP";
				case CanvasLayoutModel.CIRCO:
					return "CIRCO";
				case CanvasLayoutModel.TWOPI:
					return "TWOPI";
				case CanvasLayoutModel.OSAGE:
					return "OSAGE";
				case CanvasLayoutModel.PATCHWORK:
					return "PATCHWORK";
			}
			return "";

		}
	}
	public class Canvas: Gtk.Widget, IterableWidget, WidgetWithSelection {
		// Public properties
		public bool resizeAllowed { get; set; default= true;}
		public bool movementAllowed { get; set; default= true;}
		public bool clickSelectionAllowed { get; set; default= true;}
		public ConnectionZLocation connectionRenderPosition { get; set; default= ConnectionZLocation.UNDER;}

		// Key reaction
		private Gtk.EventControllerKey keyController;

		// Ordering
		protected Gee.LinkedList< Gtk.Widget> zOrder;
		protected Gee.LinkedList< Gtk.Widget> pendingRaising;

		// WidgetWithSelection implementation
		internal Gee.HashSet<Gtk.Widget> selectableWidgets { get; set;}
		protected Gee.HashSet< Gtk.Widget> selectionProtected { get; set;}
		public Gee.HashSet< Gtk.Widget> selection {
			get {
				return this.selectionProtected;
			}
		}

		// Connection shit
		private Gee.HashSet< Connection> connections;

		public CanvasView? view= null;

		/**
		* A reject-ed connect-tion can be mark-ed for retain-tion with the variable on the connect-tion
		*/
		public signal void onConnectionDragEnd(
			bool releaseOnConnectionEnd,

			Gee.HashSet< Connection>? accepted,
			Gee.HashSet< Connection>? selfConnectionProposedRejection,
			Gee.HashSet< Connection>? nonAcceptanceRejection,

			Gee.HashSet< Connection>? releaseOnCanvasRejection
		);

		public bool squapesOver= false;
		public DebugSquape[]? debugSquapes= null;
 
		static construct {
			set_css_name( "Canvas");
		}
		public Canvas() {
			this.zOrder= new Gee.LinkedList< Gtk.Widget>();
			this.pendingRaising= new Gee.LinkedList< Gtk.Widget>();
			this.set_can_focus( true);
			this.set_focus_on_click( true);
			this.set_focusable( true);
			this.setupSelection();
			this.keyController= new Gtk.EventControllerKey();
			this.keyController.key_pressed.connect( this.keyPressed);
			this.add_controller( this.keyController);

			this._selectableWidgets= getNewWidgetSet();
			this.connections= new Gee.HashSet< Connection>(
				Connection.hash
			);
		}

		internal bool getNewSelectionStatus(
			Gee.HashSet< Gtk.Widget> testingSet,
			WidgetWithSelection.WidgetSelectionAlterationMode alterationMode,
			out Gee.HashSet< Gtk.Widget> newlySelected,
			out Gee.HashSet< Gtk.Widget> newlyUnSelected
		) {
			return this.alterSetWithWidgets(
				ref this._selectionProtected,
				testingSet,
				alterationMode,
				false,
				out newlySelected,
				out newlyUnSelected,
				null,
				null
			);
		}
		
		private bool keyPressed(
			Gtk.EventControllerKey controller,
			uint keyval,
			uint keycode,
			Gdk.ModifierType modifierState
		) {
			// if( this.boundDragData!= null) {
			// 	return false;
			// }
			if( modifierState== Gdk.ModifierType.CONTROL_MASK) {
				if( keycode== 38) {
					this.alterSelectionWholly(
						WidgetWithSelection.WholeSelectionAlterationMode.SELECT
					);
					return true;
				} else if( keycode== 57) {
					this.alterSelectionWholly(
						WidgetWithSelection.WholeSelectionAlterationMode.UNSELECT
					);
					return true;
				} else if( keycode== 31) {
					// print( "SELECTION BEFORE:: SIZE: %i, MEMBER-S:\n", this.selectionProtected.size);
					// foreach( Gtk.Widget widget in this.selectionProtected) {
					// 	print( "%s, ", widget.name);
					// }
					// print( "\n");
					this.alterSelectionWholly(
						WidgetWithSelection.WholeSelectionAlterationMode.INVERT
					);
					// print( "SELECTION AFTER:: SIZE: %i, MEMBER-S:\n", this.selectionProtected.size);
					// foreach( Gtk.Widget widget in this.selectionProtected) {
					// 	print( "%s, ", widget.name);
					// }
					// print( "\n");
					this.raiseWidgets(
						this.selectionProtected,
						false
					);
					return true;
				}

			}
			return false;
		}
		public bool getNodeSelectionMembership(
			NodeWrapper node
		) {
			return this.selectionProtected.contains( node);
		}
		/**
		* Desire to raise multiple widget-s in their existing order
		* one can scroll through zOrder and then pendingRaising ( if not all handled)
		* if the current is in the toBeRaised then raise it
		*/
		internal void raiseWidgets(
			Gee.HashSet< Gtk.Widget> widgets,
			bool delayed
		) {
			int length= widgets.size;
			if( length< 2) {
				if( length== 0) {
					return;
				} else {
					Gee.Iterator< Gtk.Widget> widgetsIterator= widgets.iterator();
					widgetsIterator.next();
					this.raiseWidget(
						widgetsIterator.get(),
						delayed
					);
					return;
				}
			}
			Gtk.Widget[] removals= new Gtk.Widget[ length];
			int zOrderRemovedCount= 0;
			foreach( Gtk.Widget contained in this.zOrder) {
				if( widgets.contains( contained)) {
					removals[ zOrderRemovedCount]= contained;
					zOrderRemovedCount++;
				}
			}
			if( zOrderRemovedCount!= length) {
				int currentMaxRemovalIndex= zOrderRemovedCount;
				foreach( Gtk.Widget contained in this.pendingRaising) {
					if( widgets.contains( contained)) {
						removals[ currentMaxRemovalIndex]= contained;
						currentMaxRemovalIndex++;
					}
				}
			}
			Gtk.Widget removed;
			for( int iteration= 0; iteration< length; iteration++) {
				removed= removals[ iteration];
				if( iteration< zOrderRemovedCount) {
					this.zOrder.remove( removed);
				} else {
					this.pendingRaising.remove( removed);
				}
				if( delayed) {
					this.pendingRaising.add( removed);
				} else {
					this.zOrder.add( removed);
					removed.unparent();
					removed.set_parent( this);
				}
			}
		}
		internal void raiseWidget(
			Gtk.Widget widget,
			bool delayed
		) {
			bool removed= this.zOrder.remove( widget);
			if( !removed) {
				this.pendingRaising.remove( widget);
			}
			if( delayed) {
				this.pendingRaising.add( widget);
			} else {
				this.zOrder.add( widget);
				widget.unparent();
				widget.set_parent( this);
			}
		}
		internal void delayedRaiseFinalize() {
			foreach( Gtk.Widget widget in this.pendingRaising) {
				this.zOrder.add( widget);
				widget.unparent();
				widget.set_parent( this);
			}
			pendingRaising.clear();
			// HACK
			// The unparenting can mess up focus but manually setting the focus seems to retain it
			this.root.set_focus( this.root.get_focus());
		}
		public NodeWrapper addNode(
			Gtk.Widget widget,
			Gsk.Transform? transformation= null
		) {
			NodeWrapper wrapper= new NodeWrapper();
			wrapper.canvas= this;
			wrapper.contained= widget;
			wrapper.set_parent( this);
			if( transformation!= null){
				wrapper.transformation= transformation;
			}
			this.zOrder.add( wrapper);
			this._selectableWidgets.add( wrapper);
			return wrapper;
		}
		public NodeWrapper? getNodeWrapper(
			Gtk.Widget widget
		) {
			NodeWrapper? wrapper= null;
			foreach( Gtk.Widget contained in this) {
				wrapper= ( NodeWrapper) contained;
				if( wrapper.contained== widget) {
					break;
				}
			}
			return wrapper;
		}
		public bool removeNode(
			Gtk.Widget widget
		) {
			NodeWrapper? wrapper= this.getNodeWrapper( widget);
			if( wrapper!= null) {
				var removalSet= getNewWidgetSet( { wrapper});
				this.alterSelectionWithWidgets(
					removalSet,
					WidgetWithSelection.WidgetSelectionAlterationMode.REMOVE
				);
				wrapper.unparent();
				bool removed= this.zOrder.remove( wrapper);
				if( !removed) {
					this.pendingRaising.remove( wrapper);
				}
				this._selectableWidgets.remove( wrapper);
				return true;
			} else {
				return false;
			}
		}

		public void snapchotConnection(
			Connection connection,
			Gtk.Snapshot snapshot
		) {
			snapshot.push_stroke(
				connection.path,
				connection.pathConfig.stroke
			);
			this.snapshot_child(
				( Gtk.Widget) connection,
				snapshot
			);
			snapshot.pop();
		}
		private void snapshotSquapes(
			Gtk.Snapshot snapshot
		) {
			if( this.debugSquapes!= null) {
				Graphene.Rect squapeBounds= Graphene.Rect();
				foreach( DebugSquape debugSquape in this.debugSquapes) {
					squapeBounds.init(
						debugSquape.x,
						debugSquape.y,
						debugSquape.width,
						debugSquape.height
					);
					snapshot.append_color(
						debugSquape.color,
						squapeBounds
					);
				}
			}
		}
		public override void snapshot(
			Gtk.Snapshot snapshot
		) {
			if( !this.squapesOver) {
				this.snapshotSquapes( snapshot);
			}
			if( this._connectionRenderPosition== ConnectionZLocation.UNDER) {
				foreach( Connection connection in this.connections) {
					this.snapchotConnection(
						connection,
						snapshot
					);
				}
			}
			foreach( Gtk.Widget contained in this.zOrder) {
				this.snapshot_child(
					contained,
					snapshot
				);
			}
			foreach( Gtk.Widget contained in this.pendingRaising) {
				this.snapshot_child(
					contained,
					snapshot
				);
			}
			if( this._connectionRenderPosition== ConnectionZLocation.OVER) {
				foreach( Connection connection in this.connections) {
					this.snapchotConnection(
						connection,
						snapshot
					);
				}
			}
			if( this.squapesOver) {
				this.snapshotSquapes( snapshot);
			}
		}
		public override Gtk.SizeRequestMode get_request_mode() {
			return Gtk.SizeRequestMode.CONSTANT_SIZE;
		}
		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			minimum= 1;
			natural= 1;
			minimum_baseline= -1;
			natural_baseline= -1;
		}
		public override void size_allocate(
			int width,
			int height,
			int baseline
		) {
			/*
			No change-s must occur when the canvas is allocated a new size, the canvas should never have size constraint-s due to it being placed
			in a viewer, should a canvas be placed not in a viewer then one may wish to squash all contain-ed node-s into the available space
			they may also not wish this
			it is not work for now
			contained widget-s must still be allocated
			*/
			Gtk.Requisition naturalSize;
			NodeWrapper wrapper;
			foreach( Gtk.Widget contained in this.zOrder) {
				wrapper= ( NodeWrapper) contained;
				contained.get_preferred_size(
					null,
					out naturalSize
				);
				contained.allocate(
					naturalSize.width,
					naturalSize.height,
					-1,
					wrapper.transformation
				);
			}
			foreach( Gtk.Widget contained in this.pendingRaising) {
				wrapper= ( NodeWrapper) contained;
				contained.get_preferred_size(
					null,
					out naturalSize
				);
				contained.allocate(
					naturalSize.width,
					naturalSize.height,
					-1,
					wrapper.transformation
				);
			}
			foreach( Connection connection in this.connections) {
				this.allocateConnection( connection);
			}
		}

		// Connection shit
		public Connection? formConnection(
			ConnectionEnd end0,
			ConnectionEnd end1,
			owned Connection? connection= null
		) {
			// So i believe that having this be owned means that this function can not be passed an un-owned reference
			if( connection== null) {
				connection= new Connection();
			} else {
				if( connection.end0== end0) {
					if( connection.end1== end1) {
						return null;
					}
				}
			}
			bool acceptance;
			acceptance= end0.acceptsConnection(
				InteractionSource.PROGRAMMATIC,
				end1,
				connection
			);
			acceptance= acceptance&& end1.acceptsConnection(
				InteractionSource.PROGRAMMATIC,
				end0,
				connection
			);
			if( acceptance) {
				end0.connectionsInternal.add( connection);
				end1.connectionsInternal.add( connection);
				connection._end0= end0;
				connection._end1= end1;
				this.connections.add( connection);
				// connection.set_parent( this);
				connection.insert_after(
					this,
					null
				);
				// this.allocateConnection( connection);
				return connection;
			} else {
				return null;
			}

		}
		public void removeConnection(
			Connection connection
		) {
			this.connections.remove( connection);
			if( connection._end0!= null) {
				connection._end0.connectionsInternal.remove( connection);
			}
			if( connection._end1!= null) {
				connection._end1.connectionsInternal.remove( connection);
			}
			connection.unparent();
		}
		public Gee.HashSet< Connection> removeAllConnections(
			ConnectionEnd end
		) {
			Gee.HashSet< Connection> connectionsCopy= new Gee.HashSet< Connection>(
				Connection.hash
			);
			connectionsCopy.add_all( end.connectionsInternal);
			foreach( Connection connection in connectionsCopy) {
				this.removeConnection( connection);
			}
			return connectionsCopy;
		}

		internal void allocateConnection(
			Connection connection
		) {
			// Graphene.Point min= Graphene.Point();
			// Graphene.Point max= Graphene.Point();
			// if( connection.end0.ownPositionCanvas== null) {
			// 	return;
			// }
			// if( connection.end1.ownPositionCanvas== null) {
			// 	return;
			// }
			// if( connection.end0.ownPositionCanvas.x< connection.end1.ownPositionCanvas.x) {
			// 	min.x= connection.end0.ownPositionCanvas.x;
			// 	max.x= connection.end1.ownPositionCanvas.x;
			// } else {
			// 	min.x= connection.end1.ownPositionCanvas.x;
			// 	max.x= connection.end0.ownPositionCanvas.x;
			// }
			// if( connection.end0.ownPositionCanvas.y< connection.end1.ownPositionCanvas.y) {
			// 	min.y= connection.end0.ownPositionCanvas.y;
			// 	max.y= connection.end1.ownPositionCanvas.y;
			// } else {
			// 	min.y= connection.end1.ownPositionCanvas.y;
			// 	max.y= connection.end0.ownPositionCanvas.y;
			// }
			// Gsk.Transform transform= new Gsk.Transform();
			// transform= transform.translate( min);
			// int width, height;
			// width= ( int) ( max.x- min.x);
			// height= ( int) ( max.y- min.y);
			// print( "( %f, %f), ( %i, %i)\n", min.x, min.y, width, height);
			// connection.allocate(
			// 	width,
			// 	height,
			// 	-1,
			// 	transform
			// );
			connection.recreatePath();
			Graphene.Rect pathBounds;
			connection.path.get_stroke_bounds(
				connection.pathConfig.stroke,
				out pathBounds
			);
			Gsk.Transform transform= new Gsk.Transform();
			transform= transform.translate( pathBounds.origin);
			connection.allocate(
				( int) pathBounds.size.width,
				( int) pathBounds.size.height,
				-1,
				transform
			);

			// 
			// box.init_from_points(
				
			// )
		}
		internal ConnectionEndDragData? connectionEndDragData= null;
		internal void connectionEndDragStart(
			ConnectionEnd connectionEnd,
			ConnectionEndDragType dragType,
			Gtk.GestureDrag dragController,
			double startX,
			double startY
		) {
			this.connectionEndDragData= ConnectionEndDragData();
			this.connectionEndDragData.type= dragType;
			this.connectionEndDragData.startConnectionEnd= connectionEnd;
			this.connectionEndDragData.cursorPosition= Graphene.Point();
			this.connectionEndDragData.hoveredTarget= null;
			this.connectionEndDragData.acceptedConnections= new Gee.HashSet< Connection>( Connection.hash);
			this.connectionEndDragData.selfConnectProposedRejection= new Gee.HashSet< Connection>( Connection.hash);
			this.connectionEndDragData.nonAcceptanceRejection= new Gee.HashSet< Connection>( Connection.hash);
			this.getLocalPointerPosition(
				dragController.get_current_event_device(),
				out this.connectionEndDragData.cursorPosition
			);
			
			this.connectionEndDragData.connections= new Gee.HashSet< Connection>( Connection.hash);
			if ( dragType== ConnectionEndDragType.CREATE) {
				Connection connection= new Connection();
				connectionEnd.connectionsInternal.add( connection);
				connection._end0= connectionEnd;
				connection.oneEndDragged= true;
				this.connections.add( connection);
				connection.insert_after(
					this,
					null
				);
				this.connectionEndDragData.connections.add( connection);
			} else {
				this.connectionEndDragData.connections.add_all( connectionEnd.connectionsInternal);
				connectionEnd.connectionsInternal.clear();
				foreach( Connection connection in this.connectionEndDragData.connections) {
					connection.removeEnd( connectionEnd);
					connection.oneEndDragged= true;
				}
			}


		}
		internal void onConnectionTargetHover(
			ConnectionEnd connectionEnd
		) {
			connectionEnd.add_css_class( "connectionTargetHover");
			this.connectionEndDragData.hoveredTarget= connectionEnd;
			connectionEnd.connectionTargetHover= true;
			bool accepted;
			bool passOccured= false;
			bool rejectionOccured= false;
			bool selfConnectProposalOccured= false;
			string acceptanceClass;
			ConnectionEnd existingConnectedEnd;
			foreach( Connection connection in this.connectionEndDragData.connections) {
				connection.add_css_class( "connectionTargetHover");
				if( connection.end0!= null) {
					existingConnectedEnd= connection.end0;
				} else {
					existingConnectedEnd= connection.end1;
				}
				if ( existingConnectedEnd== connectionEnd) {
					acceptanceClass= "rejected";
					connection.add_css_class( "selfConnectProposed");
					accepted= false;
					this.connectionEndDragData.selfConnectProposedRejection.add( connection);
					if( !selfConnectProposalOccured) {
						selfConnectProposalOccured= true;
						connectionEnd.add_css_class( "selfConnectProposed");
					}
				} else {
					accepted= connectionEnd.acceptsConnection(
						InteractionSource.GUI,
						this.connectionEndDragData.startConnectionEnd,
						connection
					);
					if( accepted) {
						acceptanceClass= "accepted";
						this.connectionEndDragData.acceptedConnections.add( connection);
					} else {
						acceptanceClass= "rejected";
						this.connectionEndDragData.nonAcceptanceRejection.add( connection);
					}
				}
				connection.add_css_class( acceptanceClass);
				passOccured= passOccured|| accepted;
				rejectionOccured= rejectionOccured|| ( !accepted);
			}
			if( passOccured&& rejectionOccured) {
				acceptanceClass= "partialAccept";
			} else if( passOccured) {
				acceptanceClass= "fullAccept";
			} else { // if( rejectionOccured)
				acceptanceClass= "noAccept";
			}
			connectionEnd.add_css_class( acceptanceClass);
		}
		internal void onConnectionTargetExit(
			ConnectionEnd connectionEnd,
			bool refreshInsteadOfWipe
		) {
			connectionEnd.remove_css_class( "connectionTargetHover");
			foreach( Connection connection in this.connectionEndDragData.connections) {
				connection.remove_css_class( "connectionTargetHover");
				connection.remove_css_class( "accepted");
				connection.remove_css_class( "rejected");
				connection.remove_css_class( "selfConnectProposed");
			}
			connectionEnd.remove_css_class( "partialAccept");
			connectionEnd.remove_css_class( "fullAccept");
			connectionEnd.remove_css_class( "noAccept");
			connectionEnd.remove_css_class( "selfConnectProposed");
			connectionEnd.connectionTargetHover= false;
			if( refreshInsteadOfWipe) {
				this.connectionEndDragData.hoveredTarget= null;
				this.connectionEndDragData.acceptedConnections.clear();
				this.connectionEndDragData.selfConnectProposedRejection.clear();
				this.connectionEndDragData.nonAcceptanceRejection.clear();
			} else {
				this.connectionEndDragData= null;
			}
		}
		internal void connectionEndDragUpdate(
			ConnectionEnd connectionEnd,
			Gtk.GestureDrag dragController,
			double offsetX,
			double offsetY
		) {
			this.getLocalPointerPosition(
				dragController.get_current_event_device(),
				out this.connectionEndDragData.cursorPosition
			);
			Gtk.Widget? under= this.pick(
				( double) this.connectionEndDragData.cursorPosition.x,
				( double) this.connectionEndDragData.cursorPosition.y,
				Gtk.PickFlags.DEFAULT
			);
			if( this.connectionEndDragData.hoveredTarget!= null) {
				if( under!= this.connectionEndDragData.hoveredTarget) {
					// diconnect
					this.onConnectionTargetExit(
						this.connectionEndDragData.hoveredTarget,
						true
					);
					if( under!= null) {
						if( under is ConnectionEnd) {
							// connect
							this.onConnectionTargetHover(
								( ConnectionEnd) under
							);
						}
					}
				}
			} else {
				if( under!= null) {
					if( under is ConnectionEnd) {
						// connect
						this.onConnectionTargetHover(
							( ConnectionEnd) under
						);
					}
				}
			}
			foreach( Connection connection in this.connectionEndDragData.connections) {
				this.allocateConnection( connection);
			}
		}
		internal void connectionEndDragEnd(
			ConnectionEnd connectionEnd,
			Gtk.GestureDrag dragController,
			double offsetX,
			double offsetY
		) {
			this.connectionEndDragData.hoveredTarget.connectionsInternal.add_all( this.connectionEndDragData.acceptedConnections);
			foreach( Connection connection in this.connectionEndDragData.acceptedConnections) {
				if( connection._end0== null) {
					connection._end0= this.connectionEndDragData.hoveredTarget;
				} else {
					connection._end1= this.connectionEndDragData.hoveredTarget;
				}
				connection.oneEndDragged= false;
			}

			// releaseOnConnectionEnd: bool
			// accepted: set< connection>?
			// self connection proposed rejection: set< connection>?
			// not accepted rejection: set< connection>?
			// releaseOnCanvasRejection: set< connection>?
			bool releaseOnConnectionEnd= this.connectionEndDragData.hoveredTarget!= null;

			Gee.HashSet< Connection>? selfConnectionProposedRejection;
			Gee.HashSet< Connection>? nonAcceptanceRejection;
			Gee.HashSet< Connection>? releaseOnCanvasRejection;
			if ( releaseOnConnectionEnd) {
				selfConnectionProposedRejection= this.connectionEndDragData.selfConnectProposedRejection;
				nonAcceptanceRejection= this.connectionEndDragData.nonAcceptanceRejection;
				releaseOnCanvasRejection= null;
			} else {
				selfConnectionProposedRejection= null;
				nonAcceptanceRejection= null;
				releaseOnCanvasRejection= new Gee.HashSet< Connection>( Connection.hash);
				releaseOnCanvasRejection.add_all( this.connectionEndDragData.connections);
				releaseOnCanvasRejection.remove_all( this.connectionEndDragData.acceptedConnections);
			}
			Gee.HashSet< Connection> allRemoved= new Gee.HashSet< Connection>( Connection.hash);
			allRemoved.add_all( this.connectionEndDragData.connections);
			allRemoved.remove_all( this.connectionEndDragData.acceptedConnections);
			foreach( Connection connection in allRemoved) {
				connection._retainRejected= false;
			}
			this.onConnectionDragEnd(
				releaseOnConnectionEnd,
				this.connectionEndDragData.acceptedConnections,
				selfConnectionProposedRejection,
				nonAcceptanceRejection,
				releaseOnCanvasRejection
			);
			foreach( Connection connection in allRemoved) {
				if( connection.retainRejected) {
					connection._retainRejected= null;
				} else {
					this.removeConnection( connection);
				}
			}
			this.onConnectionTargetExit(
				this.connectionEndDragData.hoveredTarget,
				false
			);
		}
		internal void getLocalPointerPosition(
			Gdk.Device pointerDevice,
			out Graphene.Point point
		) {
			double pointerX, pointerY;
			pointerDevice.get_surface_at_position(
				out pointerX,
				out pointerY
			);
			point.init(
				( float) pointerX,
				( float) pointerY
			);
			this.root.compute_point(
				this,
				point,
				out point
			);
		} 
		public CanvasLayoutArgs layoutArgs { get; set; default= CanvasLayoutArgs.default();}
		private float pointsToInchesF(
			float pixelsValue
		) {
			return pixelsValue/ 72.0f;
		}
		private float inchesToPointsF(
			float inchesValue
		) {
			return inchesValue* 72.0f;
		}
		private int pointsToInchesI(
			int pixelsValue
		) {
			return ( int) Math.rintf( ( float) pixelsValue/ 72.0f);
		}
		private int inchesToPointsI(
			int inchesValue
		) {
			return ( int) Math.rintf( ( float) inchesValue* 72.0f);
		}
		private void printNodeSizes(
			Gee.HashSet< NodeWrapper> nodes
		) {
			string outputWidthS, outputHeightS;
			float width, height;
			foreach( NodeWrapper node in nodes) {
				Gvc.Node gVNode= node.layoutCalculateTionData.graphvizNode;
				outputWidthS= node.layoutCalculateTionData.graphvizNode.get( "width");
				outputHeightS= node.layoutCalculateTionData.graphvizNode.get( "height");
				if( ( outputWidthS!= null)&& ( outputHeightS!= null)) {
					width= this.inchesToPointsF( float.parse( outputWidthS));
					height= this.inchesToPointsF( float.parse( outputHeightS));
					print(
						"id: %i, width: %f, height: %f\n",
						node.count,
						width,
						height
					);
				} else {
					print( "fail: id: %i\n", node.count);
				}
			}
		}

		/*
		* the node arg that request-s retain-tion of minimum size is broken, the measure and allocate-tion of both the nodewrapper bin and it~s contain-ed bin need to be inspect-ed, the minimum of the node should be the minimum of the contained if ( contained sizing) or ( own sizing and no resize past minimum), and 0, 0 otherwise, the minimum should then be set accordingly
		*/
		public void layout(
			Gee.HashSet< NodeWrapper> nodes,
			CanvasLayoutModel model= CanvasLayoutModel.DOT,
			CanvasLayoutArgs? layoutArgs= null,
			NodeLayoutArgs? nodeArgOverride= null,
			ConnectionLayoutArgs? connectionArgOverride= null
		) throws LayoutError {
			print( "L HIT 0\n");
			if( layoutArgs== null) {
				layoutArgs= this.layoutArgs;
			}
			Gvc.Context context= new Gvc.Context();
			Gvc.Desc direction;
			if( layoutArgs.directed) {
				direction= Gvc.Agdirected;
			} else {
				direction= Gvc.Agundirected;
			}
			Gvc.Graph graph= new Gvc.Graph(
				"name",
				direction,
				0
			);
			// not concerned with graph defaults as not implement-ing sub-graph-(.) at this moment
			if( layoutArgs.maxBound!= null) {
				graph.safe_set(
					"size",
					"%i,%i".printf(
						( int) Math.rintf( this.pointsToInchesF( layoutArgs.maxBound.x)),
						( int) Math.rintf( this.pointsToInchesF( layoutArgs.maxBound.y))
					),
					""
				);
			}
			if( layoutArgs.ratioType!= null) {
				string s= "";
				bool set= true;
				if( layoutArgs.ratioType== CanvasLayoutRatio.VALUE) {
					if( layoutArgs.ratioValue== null) {
						set= false;
					} else {
						s= layoutArgs.ratioValue.to_string();
					}
				} else {
					s= layoutArgs.ratioType.to_string().down();
				}
				if( set) {
					graph.safe_set(
						"ratio",
						s,
						""
					);
				}
			}
			if( layoutArgs.overlap!= null) {
				graph.safe_set(
					"overlap",
					layoutArgs.overlap.to_string().down(),
					""
				);
			}
			if( layoutArgs.dotMinimumNodeSeperation!= null) {
				graph.safe_set(
					"nodesep",
					this.pointsToInchesF( layoutArgs.dotMinimumNodeSeperation).to_string(),
					""
				);
			}
			if( layoutArgs.neatoMode!= null) {
				graph.safe_set(
					"mode",
					layoutArgs.neatoMode.to_string().down(),
					""
				);
			}
			if( layoutArgs.neatoModel!= null) {
				graph.safe_set(
					"model",
					layoutArgs.neatoModel.to_string().down(),
					""
				);
			}
			if( layoutArgs.neatoSeed!= null) {
				graph.safe_set(
					"start",
					layoutArgs.neatoSeed.to_string(),
					""
				);
			}
			if( layoutArgs.neatoEpsilonCutoff!= null) {
				graph.safe_set(
					"epsilon",
					layoutArgs.neatoEpsilonCutoff.to_string(),
					""
				);
			}
			print( "twopiRoot null?: %b\n", layoutArgs.twopiRoot== null);
			// for some reason this is being considered as non null when call-ed from python
			// print( "HIT");
			// if( layoutArgs.twopiRoot!= null) {
			// 	if( layoutArgs.twopiRoot is NodeWrapper) { // temp if
			// 	print( "HIT");
			// 	graph.safe_set(
			// 		"root",
			// 		layoutArgs.twopiRoot.count.to_string(),
			// 		""
			// 	);
			// 	}
			// }
			if( layoutArgs.twopiRankSeperation!= null) {
				graph.safe_set(
					"ranksep",
					this.pointsToInchesF( layoutArgs.twopiRankSeperation).to_string(),
					""
				);
			}
			// if( layoutArgs.circoRoot!= null) {
			// 	if( layoutArgs.twopiRoot== null) {
			// 		graph.safe_set(
			// 			"root",
			// 			layoutArgs.circoRoot.count.to_string(),
			// 			""
			// 		);
			// 	}
			// }
			if( layoutArgs.circoMinimumNodeSeperation!= null) {
				graph.safe_set(
					"mindist",
					// docs do not specify unit, assume-ing inch-(.)
					this.pointsToInchesF( layoutArgs.circoMinimumNodeSeperation).to_string(),
					""
				);
			}
			if( layoutArgs.fdpIdealNodeSeperation!= null) {
				graph.safe_set(
					"K",
					this.pointsToInchesF( layoutArgs.fdpIdealNodeSeperation).to_string(),
					""
				);
			}
			// if( layoutArgs.fdpMaxIterations!= null) {
			// 	graph.safe_set(
			// 		"maxiter",
			// 		layoutArgs.fdpMaxIterations.to_string(),
			// 		""
			// 	);
			// }
			if( layoutArgs.fdpSeed!= null) {
				if( layoutArgs.neatoSeed== null) {
					graph.safe_set(
						"start",
						layoutArgs.fdpSeed.to_string(),
						""
					);
				}
			}
			/*
			So the scale will be accurate
			no input configures any aspect of positioning
			the bounds are returned from the lay-out
			so if center-ing, then one must find the original bounds, then center the new bounds within that
			then find the position on those new bound-(.) that the lay-out is relative to and use that to find the position of 
			okay so some things are in inches and some in points, 72 points in inch, so adjust-ment need-ed
			*/
			Graphene.Rect initialBounds= Graphene.Rect();
			bool initialBoundSet= false;
			Gvc.Node currentGVNode;
			Gee.HashSet< Connection> connections= new Gee.HashSet< Connection>( Connection.hash);
			NodeLayoutArgs? nodeArgs= nodeArgOverride;
			Gtk.Requisition currentSize;

			// this.debugSquapes= new DebugSquape[ nodes.size+ 3];
			// int squapeNum= 0;
			// Gdk.RGBA squapeCol= Gdk.RGBA();
			// squapeCol.parse( "rgba( 10%, 93%, 67%, 0.8)");
			// this.squapesOver= true;
			// Graphene.Rect origin= Graphene.Rect();
			// origin.init( -1, -1, 2, 2);
			// this.debugSquapes[ squapeNum]= DebugSquape.fromGrapheneRect(
			// 	origin,
			// 	squapeCol
			// );
			// squapeCol.parse( "rgba( 0%, 100%, 0%, 0.5)");
			// squapeNum++;
			
			print( "L HIT 1\n");
			foreach( NodeWrapper node in nodes) {
				if( node.getTargetData()== null) {
					throw new LayoutError.NODE_NOT_TARGETING_THIS_CANVAS( "");
				}
				if( node.targetData.canvas!= this) {
					throw new LayoutError.NODE_NOT_TARGETING_THIS_CANVAS( "");
				}
				if( nodeArgOverride== null) {
					nodeArgs= node.layoutArgs;
				}

				// if fix, then do not need to disturb the size-ing mode
				// if ensure-ing minimum then still desire arbitrary size above the minimum that is output, not the whims of the contain-ed, so set to own ( bin own) size-ing
				// unsure if own size-ing still sends conform-tion request-(.) to contain-ed, if not, a more advance-ed method would be to keep on contain-ed size-ing and switch to own if it does not conform
				// the desire-able out-come is for contain-ed conform-tion to the size, and adapt-tion of the bin if not
				// still calling allocate to set transform, so for fix-ed size, can still just use graphviz out-put
				// relevant to un-know-ing call-er is the condition-(.) for bin size-ing change
				Graphene.Rect nodeBounds= Graphene.Rect();
				if( node.count== 6) {
					print( "\n\npre compute bound-(.)\n");
				}
				bool boundsFound= node.compute_bounds(
					this,
					out nodeBounds
				);
				if( node.count== 6) {
					print(
						"post compute bound-(.): %f x %f + %f + %f\n\n\n",
						nodeBounds.get_width(),
						nodeBounds.get_height(),
						nodeBounds.get_x(),
						nodeBounds.get_y()
					);
				}
				// print( "id: %i, boundsFound: %b\n", node.count, boundsFound);
				if( !nodeArgs.fixCurrentSize) {
					if( node.bin.sizingActual== Bin.Sizing.CONTAINED) {
						currentSize= Gtk.Requisition();
						currentSize.width= node.bin.get_width();
						currentSize.height= node.bin.get_height();
						node.bin.ownSize= currentSize;
						node.bin.sizing= Bin.Sizing.OWN;
					}
				}

				// this.debugSquapes[ squapeNum]= DebugSquape.fromGrapheneRect(
				// 	nodeBounds,
				// 	squapeCol
				// );
				// squapeNum++;
				
				if( !initialBoundSet) {
					initialBounds= nodeBounds;
					initialBoundSet= true;
				} else {
					initialBounds= initialBounds.union(
						nodeBounds
					);
				}
				currentGVNode= graph.create_node(
					node.count.to_string(),
					1
				);
				node.layoutCalculateTionData= NodeWrapper.LayoutCalculateTionData() {
					graphvizNode= currentGVNode,
					outputBounds= Graphene.Rect()
				};
				foreach( ConnectionEnd connectionEnd in node.connectionEnds) {
					foreach( Connection connection in connectionEnd.connections) {
						if( connection.end0.targetNode in nodes) {
							if( connection.end1.targetNode in nodes) {
								connections.add( connection);
							}
						}
					}
				}
				Graphene.Point nodeCenter= nodeBounds.get_center();
				currentGVNode.safe_set(
					"pos",
					"%f,%f".printf(
						// if not setting scale flag, then input of pos is in inches and output is in points
						// maybe set scale flag for accuracy here, but dont understand it in context yet
						// invert about x for accurate represent-tion in graphviz
						this.pointsToInchesF( nodeCenter.x* -1),
						this.pointsToInchesF( nodeCenter.y)
					),
					""
				);
				if( nodeArgs.fixCurrentSize== true) {
					currentGVNode.safe_set(
						"width",
						"%f".printf(
							this.pointsToInchesF( nodeBounds.get_width())
						),
						"0"
					);
					currentGVNode.safe_set(
						"height",
						"%f".printf(
							this.pointsToInchesF( nodeBounds.get_height())
						),
						"0"
					);
					currentGVNode.safe_set(
						"fixedsize",
						"true",
						"false"
					);
				} else if( nodeArgs.respectMinimumSize== true) {
					int width, height= 0;
					node.measure(
						Gtk.Orientation.HORIZONTAL,
						-1,
						out width,
						null,
						null,
						null
					);
					node.measure(
						Gtk.Orientation.VERTICAL,
						-1,
						out height,
						null,
						null,
						null
					);
					currentGVNode.safe_set(
						"width",
						"%i".printf(
							this.pointsToInchesI( width)
						),
						"0"
					);
					currentGVNode.safe_set(
						"height",
						"%i".printf(
							this.pointsToInchesI( height)
						),
						"0"
					);
				}
				if( nodeArgs.root!= null) {
					currentGVNode.safe_set(
						"root",
						nodeArgs.root.to_string(),
						"false"
					);
				}
				if( nodeArgs.pinPosition!= null) {
					currentGVNode.safe_set(
						"pin",
						nodeArgs.pinPosition.to_string(),
						"false"
					);
				}
				if( model== CanvasLayoutModel.PATCHWORK) {
					currentGVNode.safe_set(
						"area",
						"%f".printf(
							this.pointsToInchesF( nodeBounds.get_width())* this.pointsToInchesF( nodeBounds.get_height())
						),
						"0.0"
					);
				}

			}

			// print( "\npre layout sizes:\n");
			// this.printNodeSizes( nodes);

			print( "L HIT 2\n");
			Gvc.Edge currentGVEdge;
			ConnectionLayoutArgs? connectionArgs= connectionArgOverride;
			foreach( Connection connection in connections) {

				// for detect-ing connect-tion-(.) where some node is not contain-ed in the input set
				// print( "%b", ( connection.end0.targetNode== null));
				// print( "%b", ( connection.end0.targetNode.layoutCalculateTionData== null));

				currentGVEdge= graph.create_edge(
					connection.end0.targetNode.layoutCalculateTionData.graphvizNode,
					connection.end1.targetNode.layoutCalculateTionData.graphvizNode,
					null,
					1
				);
				if( connectionArgOverride== null) {
					connectionArgs= connection.layoutArgs;
				}

				if( connectionArgs.weight!= null) {
					currentGVEdge.safe_set(
						"weight",
						"%f".printf(
							connectionArgs.weight
						),
						"1"
					);
				}
				if( connectionArgs.constraint!= null) {
					currentGVEdge.safe_set(
						"constraint",
						connectionArgs.constraint.to_string(),
						"true"
					);
				}
				if( connectionArgs.minlen!= null) {
					currentGVEdge.safe_set(
						"minlen",
						"%i".printf(
							connectionArgs.minlen
						),
						"1"
					);
				}
				if( connectionArgs.optimalLength!= null) {
					currentGVEdge.safe_set(
						"len",
						"%f".printf(
							connectionArgs.optimalLength
						),
						"1.0"
					);
				}
			}

			print( "SUB\n");
			print( "%s\n", model.get_name());
			print( "%s\n", model.get_name().down());
			context.layout(
				graph,
				model.get_name().down()
			);
			// GLib.FileStream fs= GLib.FileStream.open( "/tmp/out.xdot", "w");
			// context.render(
			// 	graph,
			// 	"xdot",
			// 	fs
			// );
			// fs= GLib.FileStream.open( "/tmp/out.jpg", "w");
			// context.render(
			// 	graph,
			// 	"jpg",
			// 	fs
			// );
			print( "SUB\n");
			uint8[] q= new uint8[ 0];
			context.render_data(
				graph,
				"dot",
				out q
			);

			// print( "\npost layout sizes:\n");
			// this.printNodeSizes( nodes);
			print( "L HIT 3\n");

			// graph bb attribute include-s render-ing of line-(.), so must manual-ly compute-ed
			Graphene.Rect outputBounds= Graphene.Rect();
			bool first= true;
			string? outputPosS;
			string[] outputPosData;
			string? outputWidthS;
			string? outputHeightS;
			float outputWidth, outputHeight;
			foreach( NodeWrapper node in nodes) {
				outputPosS= node.layoutCalculateTionData.graphvizNode.get( "pos");
				if( outputPosS== null) {
					throw new LayoutError.NO_POS_SET( "");
				}
				outputPosData= outputPosS.split( ",", -1);
				outputWidthS= node.layoutCalculateTionData.graphvizNode.get( "width");
				outputHeightS= node.layoutCalculateTionData.graphvizNode.get( "height");
				if( ( outputWidthS== null)|| ( outputHeightS== null)) {
					throw new LayoutError.NO_SIZE_SET( "");
				}
				outputWidth= this.inchesToPointsF( float.parse( outputWidthS));
				outputHeight= this.inchesToPointsF( float.parse( outputHeightS));
				// now it is the top left
				node.layoutCalculateTionData.outputBounds.init(
					( float.parse( outputPosData[ 0]))- ( outputWidth/ 2),
					( float.parse( outputPosData[ 1])* -1)- ( outputHeight/ 2),
					outputWidth,
					outputHeight
				);
				if( first) {
					outputBounds.init_from_rect( node.layoutCalculateTionData.outputBounds);
					first= false;
				} else {
					outputBounds= outputBounds.union( node.layoutCalculateTionData.outputBounds);
				}
			}

			/*
			remaining from when it was use-ing the bb attr
			string? bb= graph.get( "bb");
			if( bb== null) {
				throw new LayoutError.NO_BOUNDING_BOX_COMPUTED( "");
			}
			// bottomLeftX, bottomLeftY, topRightX, topRightY
			string[] bbData= bb.split( ",", -1);
			// string[] bbData= bb.split( " ", -1);
			float newBBWidth= float.parse( bbData[ 2])- float.parse( bbData[ 0]);
			float newBBHeight= float.parse( bbData[ 3])- float.parse( bbData[ 1]);
			*/

			// so input posit-tion of node-(.) need-(.) to be relative to the origin as if it were the bottom left, so this means: nodePosX* -1
			// if the bb attribute is not set to the actual bound of all the node-(.) then this can be calculate-ed manual-ly
			// there is also a posibility that the gv node pos-(.) are not relative to the bound bottom left but some other origin, in which case if the origin matches the input origin then this can be use-ed to retain input node relative posit-tion-(.), in any case the bound-ing box align-ment will work
			// in this case, the new posit-tion can just be set to the output point posit-tion with x invert-ed about 0
			// align the edge-(.) of the layout output bounding box-(.) to the original and then calculate node posit-tion-(.)
			// so newBottomLeftX is oldBottomLeftX+ ( ( oldWidth- newWidth)/ 2)
			// so newBottomLeftY is oldBottomLeftY- ( ( oldHeight- newHeight)/ 2)
			// so with this new locate-tion for each node center it is
			// as graphviz point-(.) are relative to the 
			// nodeCenterX= alignedBBX+ outputNodePosX
			// nodeCenterY= alignedBBY- outputNodePosY

			Graphene.Point oldBBBottomLeft= initialBounds.get_bottom_left();
			Graphene.Point newBBBottomLeft= Graphene.Point();
			// not interest-ed in the new bb bottom left of the calculated rect, i want the bottom left of the align-ed shape
			newBBBottomLeft.x= oldBBBottomLeft.x+ ( ( initialBounds.get_width()- ( outputBounds.get_width()))/ 2);
			newBBBottomLeft.y= oldBBBottomLeft.y- ( ( initialBounds.get_height()- ( outputBounds.get_height()))/ 2);
			Gsk.Transform baseTransform= new Gsk.Transform();

			// store newBBBottomLeft gsk transform, and translate to create node gsk transform-(.), if memory stuff screws up, store newBBBottomLeft point, create relevant point, and create new gsk transform-(.)
			Gsk.Transform? resultantTransform;
			Gtk.Requisition newSize;
			print( "L HIT 4\n");
			foreach( NodeWrapper node in nodes) {

				resultantTransform= baseTransform.translate(
					newBBBottomLeft
				).translate(
					node.layoutCalculateTionData.outputBounds.get_top_left()
				);

				if( resultantTransform== null) {
					throw new LayoutError.NODE_TRANSFORM_FAIL( "");
				}
				node.transformation= resultantTransform;

				newSize= Gtk.Requisition();
				newSize.width= ( int) Math.rintf( node.layoutCalculateTionData.outputBounds.get_width());
				newSize.height= ( int) Math.rintf( node.layoutCalculateTionData.outputBounds.get_height());
				// print( "setting %i to: width: %i, height: %i\n", node.count, newSize.width, newSize.height);

				if( nodeArgOverride== null) {
					nodeArgs= node.layoutArgs;
				}
				if( !nodeArgs.fixCurrentSize) {
					// account for margin
					newSize.width-= ( ( int) Math.rint( node.resizeBound))* 2;
					newSize.height-= ( ( int) Math.rint( node.resizeBound))* 2;
					node.bin.ownSize= newSize;
					node.bin.queue_resize();
				}
				// regardless of the circumstance, allocate-tion need-s to occur, to update connect-tion posit-tion correct-ly
				int naturalWidth, naturalHeight;
				node.measure(
					Gtk.Orientation.HORIZONTAL,
					-1,
					null,
					out naturalWidth,
					null,
					null
				);
				node.measure(
					Gtk.Orientation.VERTICAL,
					-1,
					null,
					out naturalHeight,
					null,
					null
				);
				node.allocate(
					naturalWidth,
					naturalHeight,
					-1,
					node.transformation
				);
				node.potentialConnectionEndUpdate();
				node.layoutCalculateTionData= null;
			}

			// Gdk.RGBA initialColor= Gdk.RGBA();
			// initialColor.parse( "rgba( 100%, 0%, 0%, 0.5)");
			// Gdk.RGBA outputColor= Gdk.RGBA();
			// outputColor.parse( "rgba( 0%, 0%, 100%, 0.5)");
			// this.debugSquapes[ squapeNum]= DebugSquape.fromGrapheneRect(
			// 	initialBounds,
			// 	initialColor
			// );
			// squapeNum++;

			// this.debugSquapes[ squapeNum]=  DebugSquape() {
			// 	x= newBBBottomLeft.x,
			// 	y= newBBBottomLeft.y- outputBounds.get_height(),
			// 	width= outputBounds.get_width(),
			// 	height= outputBounds.get_height(),
			// 	color= outputColor
			// };
			// squapeNum++;
			// this.queue_draw();

		}
	}

	public enum SurroundingWidgetEdge {
		LEFT,
		TOP,
		RIGHT,
		BOTTOM
	}
	public enum PlacementScaling {
		RELATIVE_TO_WHOLE,
		PIXELS
	}
	public struct SurroundingWidgetPlacement {
		public SurroundingWidgetEdge edge;
		public PlacementScaling locationScaling;
		public double location;
		public PlacementScaling offsetScaling;
		public double offset;
	}
	/**
	* Implement-ed to allow a widget to control the move-ment of some node on some canvas by drag-ing
	*/
	public interface DragArea: Gtk.Widget {
		/*
			When a drag area has a contained:
				( size request mode, measure) are returned from querying contained
				( allocation) are passed on to contined

			When a drag area has no contained:
				The size-ing is arbitrary
				It could be anything
				It does not make sense to have ( height for width| width for height) sizing
				so the sizing is controlled by configurable property-s
		*/
		// Currently the moved node include-s any wrapper-s, may want to change to include the wrapped-s and not the wrapper-s

		// Public property-s
		public uint? dragMoveButton {
			get {
				if( this.dragHandleDragGesture.propagation_phase== Gtk.PropagationPhase.NONE) {
					return null;
				} else {
					return this.dragHandleDragGesture.get_button();
				}
			}
			set {
				if( value== null) {
					this.dragHandleDragGesture.propagation_phase= Gtk.PropagationPhase.NONE;
				} else {
					this.dragHandleDragGesture.propagation_phase= Gtk.PropagationPhase.TARGET;
					this.dragHandleDragGesture.button= value;
				}
			}
		}
		public abstract bool selectableByClick { get; set; default= true;}
		
		protected class TargetData {
			public NodeWrapper node;
			public Canvas canvas;
		}
		internal abstract TargetData? targetData { get; set;}
		protected class DragData {
			public double previousX;
			public double previousY;
			public bool selectionMembership;
			public bool thresholdOvercome;
		}
		protected abstract DragData? dragHandleDragData { get; set;}
		protected abstract Gtk.GestureDrag dragHandleDragGesture { get; set;}
		protected abstract bool dragConsumesEvent { get; set;}
		protected abstract Gtk.GestureClick dragEndClickController { get; set;}
		protected Gtk.PropagationPhase dragPropagationPhase {
			get {
				return this.dragHandleDragGesture.get_propagation_phase();
			}
			set {
	 			this.dragHandleDragGesture.set_propagation_phase( value);
			}
		}
		public void setupDragArea() {
			this.dragHandleDragGesture= new Gtk.GestureDrag();
			this.dragHandleDragGesture.drag_begin.connect( this.dragHandleDragBegin);
			this.dragHandleDragGesture.drag_update.connect( this.dragHandleDragUpdate);
			this.dragHandleDragGesture.drag_end.connect( this.dragHandleDragEnd);
			this.add_controller( this.dragHandleDragGesture);
			this.dragEndClickController= new Gtk.GestureClick();
			this.dragEndClickController.released.connect( this.clickRelease);
			this.dragEndClickController.set_propagation_phase( Gtk.PropagationPhase.CAPTURE);
			this.add_controller( this.dragEndClickController);
			this.targetData= null;
			this.dragHandleDragData= null;
		}
		private Canvas getCanvas(
			NodeWrapper node
		) throws WidgetHierarchyOrganisation {
			Gtk.Widget? nodeContaining= node.get_parent();
			if( nodeContaining is Canvas) {
				return ( Canvas) nodeContaining;
			}
			else {
				throw new WidgetHierarchyOrganisation.WIDGET_NOT_CONTAINED_BY_CANVAS( "Target-ed widget is not contain-ed by a `Canvas`");
			}
		}
		public void setTargetNode(
			NodeWrapper target
		) throws WidgetHierarchyOrganisation {
			Canvas canvas= this.getCanvas( target);
			TargetData targetData= new TargetData();
			targetData.node= target;
			targetData.canvas= canvas;
			this.targetData= targetData;
		}
		public void targetNodeCanvasChanges() throws WidgetHierarchyOrganisation {
			Canvas canvas= this.getCanvas( this.targetData.node);
			this.targetData.canvas= canvas;
		}
		internal TargetData? getTargetData() {
			if( this.targetData!= null) {
				return this.targetData;
			}
			else {
				// Search for node and canvas by traversing upward
				Gtk.Widget? containing= this.get_parent();
				if( containing== null) {
					return null;
				}
				Gtk.Widget contained= this;
				while( true) {
					if( containing is Canvas) {
						// FOUND
						this.targetData= new TargetData();
						this.targetData.node= ( NodeWrapper) contained;
						this.targetData.canvas= ( Canvas) containing;
						return this.targetData;
					}
					else {
						Gtk.Widget? currentContaining= containing.get_parent();
						if( currentContaining== null) {
							return null;
						}
						else {
							contained= containing;
							containing= currentContaining;
						}
					}
				}
			}
		}
		private void dragHandleDragBegin(
			double start_x,
			double start_y
		) {
			if( this.dragConsumesEvent) {
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.CLAIMED);
			}
			if( this.dragHandleDragData!= null) {
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			TargetData? targetData= this.getTargetData();
			if( targetData== null) {
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			if( this.targetData.canvas.view== null) {
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			if ( !this.contains(
				start_x,
				start_y
			)) {
				// this is to prevent node wrap-er surround-ing widget-(.) from drag-ing it
				// if unsuit-able then this could be address=ed with a switch for this behave-iour or maybe some set of allow-ed or diss-allow-ed widget-(.)
				// I am tending to the logic that widget-(.) outside of widget is not common
				// otherwise the external widgets and the node can be contained within a container
				// If widget-(.) should have irregular geometry, they can surround that geometry with their bound
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			bool groupDragSucseeded= this.targetData.canvas.view.initiateGroupDrag(
				this
			);
			if( !groupDragSucseeded) {
				this.dragHandleDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.dragHandleDragData= new DragData();
			double surfaceX, surfaceY;
			this.dragHandleDragGesture.get_device().get_surface_at_position( out surfaceX, out surfaceY);
			this.dragHandleDragData.previousX= surfaceX;
			this.dragHandleDragData.previousY= surfaceY;
			this.dragHandleDragData.selectionMembership= targetData.node.getSelectionMembership();
			this.dragHandleDragData.thresholdOvercome= false;
		}
		private void dragHandleDragUpdate(
			double offset_x,
			double offset_y
		) {
			if( this.dragHandleDragData== null) {
				return;
			}

			// ok.... who did this, what is this about
			if( !this.dragHandleDragData.thresholdOvercome) {
				double travelDistance= Math.sqrt( Math.pow( offset_x, 2.0)+ Math.pow( offset_y, 2.0));
				if( travelDistance< dragThreshold) {
					return;
				}
				this.dragHandleDragData.thresholdOvercome= true;
			}
			double surfaceX, surfaceY;
			this.dragHandleDragGesture.get_device().get_surface_at_position( out surfaceX, out surfaceY);
			float xDeltaOffset= ( float) ( surfaceX- this.dragHandleDragData.previousX);
			float yDeltaOffset= ( float) ( surfaceY- this.dragHandleDragData.previousY);
			this.dragHandleDragData.previousX= surfaceX;
			this.dragHandleDragData.previousY= surfaceY;
			this.targetData.canvas.view.groupDragUpdate(
				this.dragHandleDragData.selectionMembership,
				this,
				xDeltaOffset,
				yDeltaOffset
			);
		}
		private void dragFinalize(
			Gdk.ModifierType modifierState
		) {
			if( !this.dragHandleDragData.thresholdOvercome) {
				if( this.selectableByClick) {
					if( this.targetData.canvas.clickSelectionAllowed) {
						if( this.targetData.canvas.selectableWidgets.contains( this)) {
							this.targetData.canvas.alterSelectionWithWidgets(
								getNewWidgetSet( { this.targetData.node}),
								WidgetWithSelection.getAlterationModeFromModifiers( modifierState)
							);
						}
					}
				}
			}
			this.dragHandleDragData= null;
			this.targetData.canvas.view.endGroupDrag();
		}
		private void dragHandleDragEnd(
			double offset_x,
			double offset_y
		) {
			if( this.dragHandleDragData!= null) {
				this.dragFinalize(
					this.dragHandleDragGesture.get_device().get_seat().get_keyboard().get_modifier_state()
				);
			}
		}
		private void clickRelease(
			int nPress,
			double x,
			double y
		) {
			// HACK
			// The drag does not finalize on pointer release but instead on the first pointer movement after pointer release
			// Selection must occur after pointer release so this is achieved with this controller
			// Should the drag gesture not be reset, it messes up in a manner i forgot, maybe something to do with allowing contained widget-s to operate
			if( this.dragHandleDragData!= null) {
				this.dragFinalize(
					this.dragEndClickController.get_device().get_seat().get_keyboard().get_modifier_state()
				);
				this.dragHandleDragGesture.reset();
			}
		}
	}

	public struct NodeLayoutSize {
		float width;
		float height;
	}
	public struct NodeLayoutArgs {
		bool respectMinimumSize;
		bool fixCurrentSize;
		/**
		* circo specific
		*/
		bool? root;
		/**
		* neato, fdp specific
		*/
		bool? pinPosition;
		public NodeLayoutArgs.default() {
			respectMinimumSize= true;
			fixCurrentSize= true;
			// respectMinimumSize= false;
			// fixCurrentSize= false;
			root= null;
			pinPosition= null;
		}
	}
	/**
	* The top level wrapper of a widget on a canvas
	*/
	public class NodeWrapper: Bin, DragArea {
		// Public property-s
		public uint sizeResetButton { get; set; default= 3;}
		public bool movementAllowed { get; set; default= true;}
		public bool resizableBelowContainedMinimum { get; set; default= true;}
		public Gsk.Transform transformation { get; set; default= new Gsk.Transform();}
		// public uint? getResizeButton() {
		// 	if( this.resizeDragController.propagation_phase== Gtk.PropagationPhase.NONE) {
		// 		return null;
		// 	} else {
		// 		return this.resizeDragController.get_button();
		// 	}
		// }
		// public void setResizeButton(
		// 	uint? value
		// ) {
		// 	if( value== null) {
		// 		this.resizeDragController.propagation_phase= Gtk.PropagationPhase.NONE;
		// 		this.setBinMargins( 0);
		// 		this.remove_css_class( "resizable");
		// 	} else {
		// 		this.resizeDragController.propagation_phase= Gtk.PropagationPhase.TARGET;
		// 		this.resizeDragController.button= value;
		// 		this.setBinMargins( this._resizeBoundInt);
		// 		this.add_css_class( "resizable");
		// 	}
		// }
		public bool resizeAble {
			set {
				if ( value) {
					this.resizeDragController.propagation_phase= Gtk.PropagationPhase.TARGET;
					this.setBinMargins( this._resizeBoundInt);
					this.add_css_class( "resizable");
				} else {
					this.resizeDragController.propagation_phase= Gtk.PropagationPhase.NONE;
					this.setBinMargins( 0);
					this.remove_css_class( "resizable");
				}
			}
			get {
				return this.resizeDragController.propagation_phase!= Gtk.PropagationPhase.NONE;
			}
		}
		public uint resizeButton {
			get {
				return this.resizeDragController.get_button();
			}
			set {
				// this.resizeDragController.propagation_phase= Gtk.PropagationPhase.TARGET;
				this.resizeDragController.button= value;
				this.setBinMargins( this._resizeBoundInt);
				this.add_css_class( "resizable");
			}
		}
		public bool selectable {
			get {
				DragArea.TargetData? targetData= this.getTargetData();
				if( targetData!= null) {
					return targetData.canvas.selectableWidgets.contains( this);
				} else {
					return false;
				}
			}
			set {
				DragArea.TargetData? targetData= this.getTargetData();
				if( targetData!= null) {
					if( value) {
						targetData.canvas.selectableWidgets.add( this);
					} else {
						targetData.canvas.selectableWidgets.remove( this);
					}
				}
			}
		}
		public new Gtk.Widget? contained {
			get {
				return this.bin.contained;
			}
			set {
				this.bin.contained= value;
			}
		}
		public double resizeBound {
			get {
				return this._resizeBound;
			}
			set {
				this._resizeBound= value;
				this._resizeBoundInt= ( int) value;
				this.setBinMargins( this._resizeBoundInt);
			}
		}
		public bool? getSelectionMembership() {
			DragArea.TargetData? targetData= this.getTargetData();
			if( targetData!= null) {
				return targetData.canvas.getNodeSelectionMembership( this);
			} else {
				return null;
			}
		}
		// DragArea public property implementation
		public bool selectableByClick { get; set; default= true;}

		// Class
		private class Gee.HashMap< int, string> resizeCursors;
		static construct {
			set_css_name( "NodeWrapper");
			resizeCursors= new Gee.HashMap< int, string>(
				HashFuncs.intHash
			);
			resizeCursors.set( Position.LEFT, "w-resize");
			resizeCursors.set( Position.TOP| Position.LEFT, "nw-resize");
			resizeCursors.set( Position.TOP, "n-resize");
			resizeCursors.set( Position.TOP| Position.RIGHT, "ne-resize");
			resizeCursors.set( Position.RIGHT, "e-resize");
			resizeCursors.set( Position.BOTTOM| Position.RIGHT, "se-resize");
			resizeCursors.set( Position.BOTTOM, "s-resize");
			resizeCursors.set( Position.BOTTOM| Position.LEFT, "sw-resize");
		}

		// Resizing
		private struct ResizeDragData {
			public bool translateOccur;
			public Gdk.Device resizeDevice;
			public int initialX;
			public int initialY;
			public int movementX;
			public int movementY;
			public double currentX;
			public double currentY;
			public Gtk.Requisition initialReq;
			public Gtk.Requisition newReq;
			public Graphene.Point resizePoint;
			public Gsk.Transform initialTransform;
			public int widthChange;
			public int heightChange;
			public float maxTransformX;
			public float maxTransformY;
			public int minWidth;
			public int minHeight;
			public float transformX;
			public float transformY;
		}
		private ResizeDragData? resizeDragData= null;
		private Position? currentResize= null;
		private Gtk.EventControllerMotion resizeCheckerController;
		private int _resizeBoundInt;
		private double _resizeBound;
		private Gtk.GestureDrag resizeDragController;

		// Surrounding widget
		private Gee.HashMap< Gtk.Widget, SurroundingWidgetPlacement?> surroundingWidgets;

		// Raising and size reset
		private Gtk.GestureClick clickController;

		// Focus handeling
		private Gtk.EventControllerFocus focusController;
		private bool recentClick= false;

		// Average bin moment
		internal Bin bin;

		// DragArea
		internal DragArea.TargetData? targetData { get; set;}
		protected DragArea.DragData? dragHandleDragData { get; set;}
		protected Gtk.GestureDrag dragHandleDragGesture { get; set;}
		protected bool dragConsumesEvent { get; set;}
		protected Gtk.GestureClick dragEndClickController { get; set;}

		// Connection-s
		internal Gee.HashSet< ConnectionEnd> connectionEnds;
		private static int counter= 0;
		// internal int count;
		public int count;

		public Canvas canvas;

		public NodeLayoutArgs layoutArgs { get; set; default= NodeLayoutArgs.default();}
		public struct LayoutCalculateTionData {
			Gvc.Node? graphvizNode;
			Graphene.Rect outputBounds;
		}
		internal LayoutCalculateTionData? layoutCalculateTionData= null;
		
		internal NodeWrapper() {
			this.setupDragArea();

			// would be better i think to have id in context of canvas, not globe-al
			this.count= NodeWrapper.counter;
			NodeWrapper.counter+= 1;
			
			this.overflow= Gtk.Overflow.VISIBLE;
			this.bin= new Bin();
			( ( Bin) this).contained= this.bin;
			
			this.dragPropagationPhase= Gtk.PropagationPhase.BUBBLE;
			this.dragConsumesEvent= false;
			this.resizeCheckerController= new Gtk.EventControllerMotion();
			this.resizeCheckerController.set_propagation_phase( Gtk.PropagationPhase.CAPTURE);
			this.resizeCheckerController.motion.connect( this.resizeChecker);
			this.add_controller( this.resizeCheckerController);
			this.resizeDragController= new Gtk.GestureDrag();
			this.resizeDragController.set_button( 1);
			this.resizeDragController.set_propagation_phase( Gtk.PropagationPhase.CAPTURE);
			this.resizeDragController.drag_begin.connect( this.resizeDragBegin);
			this.resizeDragController.drag_update.connect( this.resizeDragUpdate);
			this.resizeDragController.drag_end.connect( this.resizeDragEnd);
			this.add_controller( this.resizeDragController);
			this.clickController= new Gtk.GestureClick();
			this.clickController.set_propagation_phase( Gtk.PropagationPhase.CAPTURE);
			this.clickController.set_button( 0);
			this.clickController.pressed.connect( this.clickPressHandler);
			this.clickController.released.connect( this.clickReleaseHandler);
			this.add_controller( this.clickController);
			this.focusController= new Gtk.EventControllerFocus();
			this.focusController.enter.connect( this.focusEnter);
			this.add_controller( this.focusController);

			this.surroundingWidgets= new Gee.HashMap< Gtk.Widget, SurroundingWidgetPlacement?>(
				HashFuncs.widgetHashFunction
			);
			this.connectionEnds= new Gee.HashSet< ConnectionEnd>(
				ConnectionEnd.hash
			);

			this.focusable= true;
			this.can_focus= true;

			this.add_css_class( "resizable");
			this.resizeBound= 10.0;
		}

		private void setBinMargins(
			int margin
		) {
			this.bin.set_margin_start( margin);
			this.bin.set_margin_end( margin);
			this.bin.set_margin_top( margin);
			this.bin.set_margin_bottom( margin);
		}

		public void setSurroundingWidget(
			Gtk.Widget widget,
			SurroundingWidgetPlacement placement,
			bool? knownPresence= null
		) {
			this.surroundingWidgets.set(
				widget,
				placement
			);
			if( knownPresence!= true) {
				if( knownPresence== false) {
					widget.set_parent( this);
				} else {
					// if( !this.surroundingWidgets.keys.contains( widget)) {
					if( widget.get_parent()!= this) {
						widget.set_parent( this);
					}
				}
			}
			this.queue_allocate();
		}

		public void removeSurroundingWidget(
			Gtk.Widget widget
		) {
			bool changed= this.surroundingWidgets.unset( widget);
			if( changed) {
				widget.unparent();
			}
		}
		// Measure result-s is un-changed by surrounding widget-s
		// Allocation need-s to be made
		// Snapshot-s should be unchanged
		private double getPixelValueInSpace(
			PlacementScaling space,
			double inputValue,
			double ownDimensionSize
		) {
			switch( space) {
				case( PlacementScaling.PIXELS):
					return inputValue;
				case( PlacementScaling.RELATIVE_TO_WHOLE):
					return inputValue* ownDimensionSize;
			}
			return 0.0;
		}
		private void getTransformFromPlacement(
			SurroundingWidgetPlacement placement,
			double ownWidth,
			double ownHeight,
			float surroundingWidth,
			float surroundingHeight,
			out float x,
			out float y
		) {
			double pixelLocation, pixelOffset;
			switch( placement.edge) {
				case( SurroundingWidgetEdge.LEFT):
					pixelLocation= this.getPixelValueInSpace(
						placement.locationScaling,
						placement.location,
						ownHeight
					);
					pixelOffset= this.getPixelValueInSpace(
						placement.offsetScaling,
						placement.offset,
						ownWidth
					);
					y= ( float) ( ( pixelLocation* -1)+ ownHeight);
					x= ( float) ( pixelOffset* -1);
					x-= surroundingWidth;
					y-= surroundingHeight/ 2;
					break;
				case( SurroundingWidgetEdge.TOP):
					pixelLocation= this.getPixelValueInSpace(
						placement.locationScaling,
						placement.location,
						ownWidth
					);
					pixelOffset= this.getPixelValueInSpace(
						placement.offsetScaling,
						placement.offset,
						ownHeight
					);
					x= ( float) pixelLocation;
					y= ( float) ( pixelOffset* -1);
					y-= surroundingHeight;
					x-= surroundingWidth/ 2;
					break;
				case( SurroundingWidgetEdge.RIGHT):
					pixelLocation= this.getPixelValueInSpace(
						placement.locationScaling,
						placement.location,
						ownHeight
					);
					pixelOffset= this.getPixelValueInSpace(
						placement.offsetScaling,
						placement.offset,
						ownWidth
					);
					y= ( float) pixelLocation;
					x= ( float) ( pixelOffset+ ownWidth);
					y-= surroundingHeight/ 2;
					break;
				case( SurroundingWidgetEdge.BOTTOM):
					pixelLocation= this.getPixelValueInSpace(
						placement.locationScaling,
						placement.location,
						ownWidth
					);
					pixelOffset= this.getPixelValueInSpace(
						placement.offsetScaling,
						placement.offset,

						ownHeight
					);
					y= ( float) ( pixelOffset+ ownHeight);
					x= ( float) ( ( pixelLocation* -1)+ ownWidth);
					x-= surroundingWidth/ 2;
					break;
			}
		}
		public override void size_allocate(
			int width,
			int height,
			int baseline
		) {
			Gtk.Widget surrounding;
			SurroundingWidgetPlacement placement;
			Gtk.Requisition minimumSize, naturalSize;
			double ownWidth= ( double) width;
			double ownHeight= ( double) height;
			float transformX, transformY;
			Graphene.Point point= Graphene.Point();
			Gsk.Transform transform= new Gsk.Transform();
			foreach ( Gee.Map.Entry< Gtk.Widget, SurroundingWidgetPlacement?> surroundingEntry in this.surroundingWidgets) {
				surrounding= surroundingEntry.key; // locator
				placement= surroundingEntry.value;
				surrounding.get_preferred_size(
					out minimumSize,
					out naturalSize
				);
				this.getTransformFromPlacement(
					placement,
					ownWidth,
					ownHeight,
					( float) naturalSize.width,
					( float) naturalSize.height,
					out transformX,
					out transformY
				);
				point.x= transformX;
				point.y= transformY;
				// print( "SURROUNDING ALLOCATE\n");
				surrounding.allocate(
					naturalSize.width,
					naturalSize.height,
					-1,
					transform.translate( point)
				);
			}
			this.allocateContained(
				width,
				height,
				baseline
			);
		}

		private void focusEnter() {
			// Only raise if the focus entry is not the result of a click
			if( !this.recentClick) {
				DragArea.TargetData? targetData= this.getTargetData();
				if( targetData!= null) {
					targetData.canvas.raiseWidget(
						this,
						false
					);
				}
			}
		}
		private void clickPressHandler(
			Gtk.GestureClick controller,
			int nPress,
			double x,
			double y
		) {
			this.recentClick= true;
			DragArea.TargetData? targetData= this.getTargetData();
			if( targetData!= null) {

				targetData.canvas.raiseWidget(
					this,
					true
				);
			}
			if( this.clickController.get_current_button()== this.sizeResetButton) {
				if( this.currentResize== null) {
					this.clickController.set_state( Gtk.EventSequenceState.DENIED);
					return;
				}
				if( targetData!= null) {
					if( !targetData.canvas.resizeAllowed) {
						this.clickController.set_state( Gtk.EventSequenceState.DENIED);
						return;
					}
				}
				this.bin.animateChange(
					Bin.Sizing.CONTAINED,
					null,
					null,
					this.onAnimationChange
				);
			}
			
		}
		private bool onAnimationChange(
			double value
		) {
			this.potentialConnectionEndUpdate();
			return true;
		}
		private void clickReleaseHandler(
			Gtk.GestureClick controller,
			int nPress,
			double x,
			double y
		) {
			this.recentClick= false;
			if( this.targetData!= null) {
				this.targetData.canvas.delayedRaiseFinalize();
			}
		}
		private void resizeChecker(
			Gtk.EventControllerMotion controller,
			double x,
			double y
		) {
			if( this.resizeDragData!= null) {
				return;
			}
			if( this.dragHandleDragData!= null) {
				return;
			}
			DragArea.TargetData? targetData= this.getTargetData();
			if( targetData!= null) {
				if( !targetData.canvas.resizeAllowed) {
					if( this.currentResize!= null) {
						this.setCursorFromResizeHandle( null);
						this.currentResize= null;
					}
					return;
				}
			}
			double width= ( double) this.get_width();
			double height= ( double) this.get_height();
			// double topDist= y;
			// double leftDist= x;
			// double rightDist= width- x;
			// double bottomDist= height- y;
			// bool topCheck= topDist< this.resizeBound;
			// bool leftCheck= leftDist< this.resizeBound;
			// bool rightCheck= rightDist< this.resizeBound;
			// bool bottomCheck= bottomDist< this.resizeBound;

			// dist from wrapper bound, positive external
			double negativeResizeBound= this._resizeBound* -1;
			double topDist= ( y* -1);
			double leftDist= ( x* -1);
			double rightDist= x- width;
			double bottomDist= y- height;
			bool topCheck= ( topDist< 0)&& ( topDist> negativeResizeBound);
			bool leftCheck= ( leftDist< 0)&& ( leftDist> negativeResizeBound);
			bool rightCheck= ( rightDist< 0)&& ( rightDist> negativeResizeBound);
			bool bottomCheck= ( bottomDist< 0)&& ( bottomDist> negativeResizeBound);

			Position? resizeHandle= null;
			bool leftRightOverlap= false;
			bool topBottomOverlap= false;
			if( topCheck) {
				if( leftCheck) {
					resizeHandle= Position.TOP| Position.LEFT;
				} else if( rightCheck) {
					resizeHandle= Position.TOP| Position.RIGHT;
				} else if( bottomCheck) {
					topBottomOverlap= true;
				} else {
					resizeHandle= Position.TOP;
				}
			} else if( bottomCheck) {
				if( leftCheck) {
					resizeHandle= Position.BOTTOM| Position.LEFT;
				} else if( rightCheck) {
					resizeHandle= Position.BOTTOM| Position.RIGHT;
				} else {
					resizeHandle= Position.BOTTOM;
				}
			} else if( leftCheck) {
				if( rightCheck) {
					leftRightOverlap= true;
				} else {
					resizeHandle= Position.LEFT;
				}
			} else if( rightCheck) {
				resizeHandle= Position.RIGHT;
			}

			if( leftRightOverlap) {
				if( topBottomOverlap) {
					resizeHandle= Position.BOTTOM| Position.RIGHT;
				} else {
					resizeHandle= Position.RIGHT;
				}
			} else if( topBottomOverlap) {
				resizeHandle= Position.BOTTOM;
			}
			// if( leftRightOverlap|| topBottomOverlap) {
			// 	print( "OVERLAP\n");
			// }
			this.setCursorFromResizeHandle( resizeHandle);
			this.currentResize= resizeHandle;
		}
		private void setCursorFromResizeHandle(
			Position? resizeHandle
		) {
			string cursor;
			if( resizeHandle== null) {
				cursor= "default";
			} else {
				cursor= this.resizeCursors.get( ( int) resizeHandle);
			}
			this.set_cursor_from_name( cursor);
		}
		private void resizeDragBegin(
			double startX,
			double startY
		) {
			if( this.currentResize== null) {
				this.resizeDragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			if( this.resizeDragData!= null) {
				this.resizeDragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			DragArea.TargetData? targetData= this.getTargetData();
			if( targetData!= null) {
				if( !targetData.canvas.resizeAllowed) {
					this.resizeDragController.set_state( Gtk.EventSequenceState.DENIED);
					return;
				}
			}
			this.resizeDragController.set_state( Gtk.EventSequenceState.CLAIMED);
			if( this.bin.sizingActual== Bin.Sizing.CONTAINED) {
				// Set to OWN sizing
				// Copy current size into this.ownSize
				Gtk.Requisition currentSize= Gtk.Requisition();
				currentSize.width= this.bin.get_width();
				currentSize.height= this.bin.get_height();
				this.bin.ownSize= currentSize;
				this.bin.sizing= Bin.Sizing.OWN;
			}
			this.resizeDragData= ResizeDragData();
			this.resizeDragData.resizeDevice= this.resizeDragController.get_device();
			this.resizeDragData.resizeDevice.get_surface_at_position(
				out this.resizeDragData.currentX,
				out this.resizeDragData.currentY
			);
			if( targetData!= null) {
				this.resizeDragData.currentX/= this.targetData.canvas.view.scale;
				this.resizeDragData.currentY/= this.targetData.canvas.view.scale;
			}

			this.resizeDragData.initialX= ( int) this.resizeDragData.currentX;
			this.resizeDragData.initialY= ( int) this.resizeDragData.currentY;
			this.resizeDragData.initialReq= this.bin.ownSize.copy();
			this.resizeDragData.newReq= Gtk.Requisition();
			this.resizeDragData.resizePoint= Graphene.Point();
			this.resizeDragData.resizePoint.init(
				0.0f,
				0.0f
			);
			// this.resizeDragData.maxTransformX= ( float) ( this.ownSize.width- this._resizeBoundInt);
			// this.resizeDragData.maxTransformY= ( float) ( this.ownSize.height- this._resizeBoundInt);
			if( this.resizableBelowContainedMinimum) {
				this.resizeDragData.maxTransformX= ( float) this.bin.ownSize.width;
				this.resizeDragData.maxTransformY= ( float) this.bin.ownSize.height;
				// i think this can be parm-ed to allow or dis-allow size-ing past a minimal point
				// this.resizeDragData.minWidth= 0;
				// this.resizeDragData.minHeight= 0;
			} else {
				Gtk.Requisition minimumSize;
				this.bin.contained.get_preferred_size(
					out minimumSize,
					null
				);
				// Always Bin.Sizing.OWN with above code so do not need to bother with little CONTAINED implementation
				if( this.bin.ownSize.width< minimumSize.width) {
					this.resizeDragData.maxTransformX= ( float) this.bin.ownSize.width;
					this.resizeDragData.minWidth= 0;
				} else {
					this.resizeDragData.minWidth= minimumSize.width;
					this.resizeDragData.maxTransformX= ( float) ( this.bin.ownSize.width- minimumSize.width);
				}
				if( this.bin.ownSize.height< minimumSize.height) {
					this.resizeDragData.maxTransformY= ( float) this.bin.ownSize.height;
					this.resizeDragData.minHeight= 0;
				} else {
					this.resizeDragData.minHeight= minimumSize.height;
					this.resizeDragData.maxTransformY= ( float) ( this.bin.ownSize.height- minimumSize.height);
				}
			}

			this.resizeDragData.widthChange= 0;
			this.resizeDragData.heightChange= 0;
			this.resizeDragData.transformX= 0.0f;
			this.resizeDragData.transformY= 0.0f;
			Gsk.Transform blankTransform= new Gsk.Transform();
			this.resizeDragData.initialTransform= blankTransform.transform( this.transformation);
			// Set update function here
			if( Position.TOP in this.currentResize) {
				if( Position.LEFT in this.currentResize) {
					this.resizeDragData.translateOccur= true;
				} else if( Position.RIGHT in this.currentResize) {
					this.resizeDragData.translateOccur= true;
				} else {
					this.resizeDragData.translateOccur= true;
				}
			} else if( Position.BOTTOM in this.currentResize) {
				if( Position.LEFT in this.currentResize) {
					this.resizeDragData.translateOccur= true;
				} else if( Position.RIGHT in this.currentResize) {
					this.resizeDragData.translateOccur= false;
				} else {
					this.resizeDragData.translateOccur= false;
				}
			} else if( this.currentResize== Position.LEFT) {
				this.resizeDragData.translateOccur= true;
			} else { // ResizeHandle.RIGHT
				this.resizeDragData.translateOccur= false;
			}
		}
		private void resizeDragUpdate(
			double xOffset,
			double yOffset
		) {
			// So movement toward top or left will require the transform to be translated by the negative of the amount and the req should be increased by that amount
			// We can store the initial req and 
			this.resizeDragData.resizeDevice.get_surface_at_position(
				out this.resizeDragData.currentX,
				out this.resizeDragData.currentY
			);
			this.resizeDragData.currentX/= this.targetData.canvas.view.scale;
			this.resizeDragData.currentY/= this.targetData.canvas.view.scale;

			this.resizeDragData.movementX= ( ( int) this.resizeDragData.currentX)- this.resizeDragData.initialX;
			this.resizeDragData.movementY= ( ( int) this.resizeDragData.currentY)- this.resizeDragData.initialY;
			bool transforming;
			Gsk.Transform? newTransform;
			Gtk.Requisition newSize;
			calculateWidgetResize(
				this.currentResize,
				this.resizeDragData.initialTransform,
				this.resizeDragData.initialReq,
				this.resizeDragData.movementX,
				this.resizeDragData.movementY,
				this.resizeDragData.maxTransformX,
				this.resizeDragData.maxTransformY,
				this.resizeDragData.minWidth,
				this.resizeDragData.minHeight,
				true,
				out newSize,
				out newTransform,
				out transforming
			);
			if( transforming) {
				this.transformation= newTransform;
			}
			this.bin.ownSize= newSize.copy();
			this.bin.queue_resize();
			this.potentialConnectionEndUpdate();
			// this.queue_resize();
			// this.queue_allocate();
			// this.queue_draw();
			// Gtk.Requisition naturalSize;
			// this.bin.get_preferred_size(
			// 	null,
			// 	out naturalSize
			// );
			// this.allocateContained(
			// 	naturalSize.width,
			// 	naturalSize.height,
			// 	-1
			// );
			// this.allocate(
			// 	-1,
			// 	newSize.width,
			// 	newSize.height,
			// 	this.transformation
			// );
			// this.queue_draw();
			// this.bin.queue_resize();

		}
		private void resizeDragEnd(
			double xOffset,
			double yOffset
		) {
			if( resizeDragData!= null) {
				this.resizeDragData= null;
			}
		}

		internal void potentialConnectionEndUpdate(
			Gee.HashSet< ConnectionEnd>? connectionEnds= null
		) {
			// Recalculate end to canvas transform matrix
			// Accumulate all unique connection-s
			if( connectionEnds== null) {
				connectionEnds= this.connectionEnds;
			}
			if( connectionEnds.size== 0) {
				return;
			}
			Gee.HashSet< Connection> connections= new Gee.HashSet< Connection>( Connection.hash);
			foreach( ConnectionEnd connectionEnd in connectionEnds) {
				connectionEnd.invalidateThisToCanvasTransform();
				connections.add_all( connectionEnd.connections);
			}
			foreach( Connection connection in connections) {
				this.canvas.allocateConnection( connection);
			}
		}

		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			base.measure(
				orientation,
				for_size,
				out minimum,
				out natural,
				out minimum_baseline,
				out natural_baseline
			);
			// print( "\nmeasure: %i, %i, %i, %i\n", minimum, natural, minimum_baseline, natural_baseline);
		}
	}

	/**
	* A simple realise-tion of DragArea
	*/
	public class DragWidget: Bin, DragArea {
		// DragArea public property implementation
		public bool selectableByClick { get; set; default= true;}

		internal DragArea.TargetData? targetData { get; set;}
		protected DragArea.DragData? dragHandleDragData { get; set;}
		protected Gtk.GestureDrag dragHandleDragGesture { get; set;}
		protected Gtk.GestureClick dragEndClickController { get; set;}
		public bool dragConsumesEvent { get; set; default= false;}
		public DragWidget() {
			this.setupDragArea();
			this.dragPropagationPhase= Gtk.PropagationPhase.TARGET;
		}
	}


	public enum SelectionShapeInfluence {
		INTERSECTION,
		CONTAINS
	}
	/**
	* The view in to the canvas
	*/
	public class CanvasView: Gtk.Widget {
		public Gsk.Transform transformation { get; set; default= new Gsk.Transform();}
		public float continuousScrollScale { get; set; default= 0.01f;}
		public float discreteScrollScale { get; set; default= 0.1f;}
		public bool calculateResultantSelectionDuringDrag { get; set; default= true;}
		public uint? getSelectionMovementButton() {
			if( this.selectionMovingDragController.propagation_phase== Gtk.PropagationPhase.NONE) {
				return null;
			} else {
				return this.selectionMovingDragController.get_button();
			}
		}
		public void setSelectionMovementButton(
			uint? value
		) {
			if( value== null) {
				this.selectionMovingDragController.propagation_phase= Gtk.PropagationPhase.NONE;
			} else {
				this.selectionMovingDragController.propagation_phase= Gtk.PropagationPhase.TARGET;
				this.selectionMovingDragController.button= value;
			}
		}
		public uint? getBoundSelectionButton() {
			if( this.selectionDragGesture.propagation_phase== Gtk.PropagationPhase.NONE) {
				return null;
			} else {
				return this.selectionDragGesture.get_button();
			}
		}
		public void setBoundSelectionButton(
			uint? value
		) {
			if( value== null) {
				this.selectionDragGesture.propagation_phase= Gtk.PropagationPhase.NONE;
			} else {
				this.selectionDragGesture.propagation_phase= Gtk.PropagationPhase.TARGET;
				this.selectionDragGesture.button= value;
			}
		}
		public SelectionShapeInfluence selectionShapeInfluence {
			get {
				return this._selectionShapeInfluence;
			}
			set {
				switch( value) {
					case( SelectionShapeInfluence.INTERSECTION):
						this.selectionInfluenceFunction= this.intersectionImplementation;
						break;
					case( SelectionShapeInfluence.CONTAINS):
						this.selectionInfluenceFunction= this.containsImplementation;
						break;
				}
				this._selectionShapeInfluence= value;
			}
		}
		public Canvas canvas {
			get {
				return this._canvas;
			}
			set {
				if( this._canvas== value) {
					return;
				}
				if( this._canvas!= null) {
					this._canvas.unparent();
					this._canvas.view= null;
				}
				value.set_parent( this);
				value.view= this;
				this._canvas= value;
			}
		}
		private Canvas _canvas= null;

		// Bound selection
		protected class BoundDragData {
			public Gee.HashMap< Gtk.Widget, Graphene.Rect?> consideredWidgets;
			public Graphene.Rect selectionBounds;
			public Gee.HashSet< Gtk.Widget> currentBounded;
			public Gee.HashSet< Gtk.Widget> initialSelection;
			public WidgetWithSelection.WidgetSelectionAlterationMode currentAlterationMode;
			public Graphene.Matrix viewToCanvasMatrix;
			public float originX;
			public float originY;
			public Graphene.Point placeholderPoint;
			public Gsk.Transform blankTransform;
		}
		protected BoundDragData? boundDragData;
		private Gtk.GestureDrag selectionDragGesture;
		private Gtk.EventControllerKey selectionDragModifierKeyController;
		internal CanvasSelection selectionWidget;

		
		delegate bool SelectionInfluenceFunction(
			Graphene.Rect q,
			Graphene.Rect w
		);
		private bool containsImplementation(
			Graphene.Rect q,
			Graphene.Rect w
		) {
			return q.contains_rect( w);
		}
		private bool intersectionImplementation(
			Graphene.Rect q,
			Graphene.Rect w
		) {
			return q.intersection(
				w,
				null
			);
		}
		private SelectionInfluenceFunction selectionInfluenceFunction= containsImplementation;
		private SelectionShapeInfluence _selectionShapeInfluence= SelectionShapeInfluence.CONTAINS;

		private Gtk.GestureDrag selectionMovingDragController;
		private struct SelectionMovementDragData {
			public double lastX;
			public double lastY;
		}
		private SelectionMovementDragData? selectionMovementDragData= null;

		private Gtk.EventControllerScroll zoomScrollController;
		// public double decelerationTopDerivativeChangeRate= -49125.15;
		public double decelerationTopDerivativeChangeRate= -125.15;
		public int decelerationDerivativeNum= 0;
		private struct DecelerationData {
			public bool haltRequest;
			public double[] decelerationRates;
			public double xVelocity;
			public bool xStopped;
			public double yVelocity;
			public bool yStopped;
			public int64? initialTime;
			public double previousTime;
		}
		private DecelerationData? decelerationData= null;
		private struct PanData {
			public double previousX;
			public double previousY;
			public Gdk.Cursor? initialCursor;
		}
		private PanData? panData= null;
		private Gtk.GestureDrag panDragController;
		public float scale {
			get {
				return this._scale;
			}
		}
		private float _scale= 1.0f;
		private bool groupDragOccuring;
		private struct PinchZoomData {
			public double previousScale;
		}
		PinchZoomData? pinchZoomData= null;
		private Gtk.GestureZoom pinchZoomController;

		static construct {
			set_css_name( "CanvasView");
		}
		
		public CanvasView() {
			this.selectionWidget= new CanvasSelection();
			this.selectionWidget.set_parent( this);

			this.selectionDragGesture= new Gtk.GestureDrag();
			this.selectionDragGesture.set_button( 1);
			this.selectionDragGesture.set_propagation_phase( Gtk.PropagationPhase.TARGET);
			this.selectionDragGesture.drag_begin.connect( this.selectionDragBegin);
			this.selectionDragGesture.drag_update.connect( this.selectionDragUpdate);
			this.selectionDragGesture.drag_end.connect( this.selectionDragEnd);
			this.add_controller( this.selectionDragGesture);
			this.selectionDragModifierKeyController= new Gtk.EventControllerKey();
			this.selectionDragModifierKeyController.set_propagation_phase( Gtk.PropagationPhase.NONE);
			this.selectionDragModifierKeyController.modifiers.connect( this.selectionDragModifierPressed);
			this.add_controller( this.selectionDragModifierKeyController);
			this.zoomScrollController= new Gtk.EventControllerScroll(
				Gtk.EventControllerScrollFlags.BOTH_AXES| Gtk.EventControllerScrollFlags.KINETIC
			);
			this.zoomScrollController.set_propagation_phase( Gtk.PropagationPhase.BUBBLE);
			this.zoomScrollController.scroll.connect( this.onScroll);
			this.zoomScrollController.decelerate.connect( this.scrollDecelerate);
			this.add_controller( this.zoomScrollController);
			this.panDragController= new Gtk.GestureDrag();
			this.panDragController.set_button( 2);
			this.panDragController.drag_begin.connect( this.panBegin);
			this.panDragController.drag_update.connect( this.panUpdate);
			this.panDragController.drag_end.connect( this.panEnd);
			this.add_controller( this.panDragController);
			this.selectionMovingDragController= new Gtk.GestureDrag();
			this.selectionMovingDragController.button= 3;
			this.selectionMovingDragController.propagation_phase= Gtk.PropagationPhase.TARGET;
			this.selectionMovingDragController.drag_begin.connect( this.selectionMovementDragBegin);
			this.selectionMovingDragController.drag_update.connect( this.selectionMovementDragUpdate);
			this.selectionMovingDragController.drag_end.connect( this.selectionMovementDragEnd);
			this.add_controller( this.selectionMovingDragController);
			this.pinchZoomController= new Gtk.GestureZoom();
			this.pinchZoomController.begin.connect( this.pinchZoomStart);
			this.pinchZoomController.scale_changed.connect( this.pinchZoomUpdate);
			this.pinchZoomController.end.connect( this.pinchZoomEnd);
			this.add_controller( this.pinchZoomController);

			this.selectionShapeInfluence= SelectionShapeInfluence.INTERSECTION;
			this.focusable= true;
			this.canvas= new Canvas();
		}

		/**
		* May be moved away from a scaling dynamic and toward-s perhaps z movement
		*
		* possibly still using the scale function but being more clever with it
		*/
		private void scaleCanvas(
			float scale,
			Graphene.Point scaleCenter
		) {
			this._transformation= this._transformation.translate(
				scaleCenter
			).scale(
				scale,
				scale
			);
			scaleCenter.x*= -1;
			scaleCenter.y*= -1;
			this._transformation= this._transformation.translate( scaleCenter);
			this._scale*= scale;
			this.queue_allocate();
		}
		private void panCanvas(
			float x,
			float y
		) {
			Graphene.Point point= Graphene.Point();
			point.x= x;
			point.y= y;
			this._transformation= this._transformation.translate( point);
			this.queue_allocate();
		}

		private bool onScroll(
			double dX,
			double dY
		) {
			switch( this.zoomScrollController.get_current_event_device().get_source()) {
				case( Gdk.InputSource.MOUSE):
					float scale= 1.0f;;
					switch( this.zoomScrollController.get_unit()) {
						case( Gdk.ScrollUnit.SURFACE):
							scale+= ( ( ( float) dY)* this._continuousScrollScale);
							break;
						case( Gdk.ScrollUnit.WHEEL):
							scale+= ( ( ( float) dY)* this._discreteScrollScale* -1);
							break;
					}
					Graphene.Point point;
					this._canvas.getLocalPointerPosition(
						this.zoomScrollController.get_current_event_device().get_seat().get_pointer(),
						out point
					);
					this.scaleCanvas(
						scale,
						point
					);
					return true;
				case( Gdk.InputSource.TOUCHPAD):
					this.panCanvas(
						(( ( float) dX)* -1)/ this._scale,
						(( ( float) dY)* -1)/ this._scale
					);
					return true;
			}

			return false;
		}

		private void scrollDecelerate(
			double xVelocity,
			double yVelocity
		) {
			this.haltDeceleration();
			Gdk.Device? currentDevice= this.zoomScrollController.get_current_event_device();
			if( currentDevice== null) {
				return;
			}
			switch( currentDevice.get_source()) {
				case( Gdk.InputSource.TOUCHPAD):
					this.decelerationData= DecelerationData() {
						haltRequest= false,
						decelerationRates= new double[ this.decelerationDerivativeNum],
						xVelocity= -1* xVelocity,
						xStopped= false,
						yVelocity= -1* yVelocity,
						yStopped= false,
						initialTime= null
					};
					for( int iteration= 0; iteration< this.decelerationDerivativeNum; iteration++) {
						this.decelerationData.decelerationRates[ iteration]= 0.0;
					}
					this.add_tick_callback(
						this.deceleration
					);
					break;
			}
		}
		private void haltDeceleration() {
			if( this.decelerationData!= null) {
				this.decelerationData.haltRequest= true;
			}
		}
		private bool deceleration(
			Gtk.Widget widget,
			Gdk.FrameClock frameClock
		) {
			if( this.decelerationData== null) {
				return GLib.Source.REMOVE;
			}
			if( this.decelerationData.haltRequest) {
				this.decelerationData= null;
				return GLib.Source.REMOVE;
			}
			// Get time since last frame
			double elapsedTime, intervalTime;
			int64 currentTime= frameClock.get_frame_time();
			if( this.decelerationData.initialTime== null) {
				this.decelerationData.initialTime= currentTime;
				elapsedTime= 0.0;
			} else {
				elapsedTime= ( currentTime- this.decelerationData.initialTime)/ ( double) GLib.TimeSpan.SECOND;
			}
			intervalTime= elapsedTime- this.decelerationData.previousTime;
			this.decelerationData.previousTime= elapsedTime;
			
			double velocityChange;
			if( this.decelerationDerivativeNum> 0) {
				double previousRateOfChange;
				for( int iteration= 0; iteration< this.decelerationDerivativeNum; iteration++) {
					if( iteration== 0) {
						previousRateOfChange= this.decelerationTopDerivativeChangeRate;
					} else {
						previousRateOfChange= this.decelerationData.decelerationRates[ iteration- 1];
					}
					this.decelerationData.decelerationRates[ iteration]+= previousRateOfChange* intervalTime;
				}
				velocityChange= this.decelerationData.decelerationRates[ this.decelerationDerivativeNum- 1]* intervalTime;
			} else {
				velocityChange= this.decelerationTopDerivativeChangeRate;
			}

			if( !this.decelerationData.xStopped) {
				if( this.decelerationData.xVelocity> 0) {
					this.decelerationData.xVelocity+= velocityChange;
					if( this.decelerationData.xVelocity< 0) {
						this.decelerationData.xStopped= true;
					}
				} else {
					this.decelerationData.xVelocity-= velocityChange;
					if( this.decelerationData.xVelocity> 0) {
						this.decelerationData.xStopped= true;
					}
				}
			}
			if( !this.decelerationData.yStopped) {
				if( this.decelerationData.yVelocity> 0) {
					this.decelerationData.yVelocity+= velocityChange;
					if( this.decelerationData.yVelocity< 0) {
						this.decelerationData.yStopped= true;
					}
				} else {
					this.decelerationData.yVelocity-= velocityChange;
					if( this.decelerationData.yVelocity> 0) {
						this.decelerationData.yStopped= true;
					}
				}
			}
			if( this.decelerationData.xStopped&& this.decelerationData.yStopped) {
				this.decelerationData= null;
				return GLib.Source.REMOVE;
			} else {
				float xChange= 0.0f;
				float yChange= 0.0f;
				if( !this.decelerationData.xStopped) {
					xChange= ( float) ( this.decelerationData.xVelocity* intervalTime);
				}
				if( !this.decelerationData.yStopped) {
					yChange= ( float) ( this.decelerationData.yVelocity* intervalTime);
				}
				this.panCanvas(
					xChange/ this._scale,
					yChange/ this._scale
				);
				return GLib.Source.CONTINUE;
			}

		}

		private void pinchZoomStart() {
			if( this.pinchZoomData!= null) {
				this.pinchZoomController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.pinchZoomData= PinchZoomData();
			this.pinchZoomData.previousScale= 1.0;
		}
		private void pinchZoomUpdate(
			Gtk.GestureZoom controller,
			double scale
		) {
			Graphene.Point point;
			this._canvas.getLocalPointerPosition(
				this.pinchZoomController.get_current_event_device(),
				out point
			);
			this.scaleCanvas(
				( float) ( scale/ this.pinchZoomData.previousScale),
				point
			);
			this.pinchZoomData.previousScale= scale;
		}
		private void pinchZoomEnd() {
			this.pinchZoomData= null;
		}

		private void panBegin(
			double startX,
			double startY
		) {
			if( this.panData!= null) {
				this.panDragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.panData= PanData();
			this.panData.previousX= 0.0;
			this.panData.previousY= 0.0;
			this.panData.initialCursor= this.get_cursor();
			this.set_cursor_from_name( "move");
		}
		private void panUpdate(
			double offsetX,
			double offsetY
		) {
			double xChange, yChange;
			xChange= ( offsetX- this.panData.previousX)/ this._scale;
			yChange= ( offsetY- this.panData.previousY)/ this._scale;

			this.panCanvas(
				( float) xChange,
				( float) yChange
			);
			this.panData.previousX= offsetX;
			this.panData.previousY= offsetY;
		}
		private void panEnd(
			double offsetX,
			double offsetY
		) {
			this.set_cursor( this.panData.initialCursor);
			this.panData= null;
		}

		private void nodeMovement(
			
		) {
			// Need to update connection-s
			// So all Connection-s 
			// So for whole process calculate 
		}
		internal bool initiateGroupDrag(
			DragArea? draggedWidget
		) {
			// Maybe do delayed raise on all in a dragged selection
			if( this.groupDragOccuring) {
				return false;
			}
			if( !this._canvas.movementAllowed) {
				return false;
			}
			if( draggedWidget!= null) {
				if( !draggedWidget.targetData.node.movementAllowed) {
					return false;
				}
			}
			this.groupDragOccuring= true;
			return true;
		}
		internal void groupDragUpdate(
			bool? selectionMembership,
			DragArea? dragArea,
			float xDeltaOffset,
			float yDeltaOffset
		) {
			Graphene.Point translationPoint= Graphene.Point();
			translationPoint.init(
				xDeltaOffset/ this.scale,
				yDeltaOffset/ this.scale
			);
			NodeWrapper? wrapper;
			if( selectionMembership== null) {
				selectionMembership= this.canvas.selection.contains( dragArea.targetData.node);
			}
			Gtk.Requisition naturalSize;
			if( selectionMembership) {
				foreach( Gtk.Widget widget in this.canvas.selection) {
					wrapper= widget as NodeWrapper;
					if( !wrapper.movementAllowed) {
						continue;
					}
					Gsk.Transform? transformation= wrapper.transformation.ref();
					transformation= transformation.translate( translationPoint);
					if( transformation!= null) {
						wrapper.transformation= transformation;
						wrapper.get_preferred_size(
							null,
							out naturalSize
						);
						wrapper.allocate(
							naturalSize.width,
							naturalSize.height,
							-1,
							wrapper.transformation
						);
						wrapper.potentialConnectionEndUpdate();
					}
				}
			} else {
				wrapper= ( NodeWrapper) dragArea.targetData.node;
				// I do not think single is called without a check to begin with
				// If this statement is incorrect then implement skipping here
				Gsk.Transform? transformation= wrapper.transformation.ref();
				transformation= transformation.translate( translationPoint);
				if( transformation!= null) {
					wrapper.transformation= transformation;
					wrapper.get_preferred_size(
						null,
						out naturalSize
					);
					wrapper.allocate(
						naturalSize.width,
						naturalSize.height,
						-1,
						wrapper.transformation
					);
					wrapper.potentialConnectionEndUpdate();
				}
			}
			// this.canvas.queue_allocate();
		}
		internal void endGroupDrag() {
			this.groupDragOccuring= false;
		}

		private void selectionMovementDragBegin(
			double startX,
			double startY
		) {
			bool willContinue= this.initiateGroupDrag( null);
			if( !willContinue) {
				this.selectionMovingDragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.selectionMovementDragData= SelectionMovementDragData();
			this.selectionMovementDragData.lastX= 0.0;
			this.selectionMovementDragData.lastY= 0.0;
		}
		private void selectionMovementDragUpdate(
			double xOffset,
			double yOffset
		) {
			this.groupDragUpdate(
				true,
				null,
				( float) ( xOffset- this.selectionMovementDragData.lastX),
				( float) ( yOffset- this.selectionMovementDragData.lastY)
			);
			this.selectionMovementDragData.lastX= xOffset;
			this.selectionMovementDragData.lastY= yOffset;
		}
		private void selectionMovementDragEnd(
			double xOffset,
			double yOffset
		) {
			this.endGroupDrag();
			this.selectionMovementDragData= null;
		}
		
		/**
		* Test for widget-s which are visible on the screen
		* only those are considered for selection
		* selection requires complete bounding of the desired node-s and so those that are partially on display are also not considered
		*/
		private void selectionDragBegin(
			double startX,
			double startY
		) {
			if( this.boundDragData!= null) {
				this.selectionDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.grab_focus();
			// Gtk.Widget containing= this.get_parent();
			// if( containing== null) {
			// 	this.selectionDragGesture.set_state( Gtk.EventSequenceState.DENIED);
			// 	return;
			// }
			// If the movement has momentum then halt the canvas in place here first
			this.haltDeceleration();
			Graphene.Rect viewedRegion= Graphene.Rect();
			if( !this.compute_bounds(
				this._canvas,
				out viewedRegion
			)) {
				this.selectionDragGesture.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			Gee.HashMap< Gtk.Widget, Graphene.Rect?> consideredWidgets= new Gee.HashMap< Gtk.Widget, Graphene.Rect?>(
				HashFuncs.widgetHashFunction
			);
			foreach( Gtk.Widget contained in this._canvas.selectableWidgets) {
				// Compute bound-s of contained in the co-ord space of this
				Graphene.Rect containedBounds= Graphene.Rect();
				if( !contained.compute_bounds(
					this._canvas,
					out containedBounds
				)) {
					continue;
				}
				if( this.selectionInfluenceFunction(
					viewedRegion,
					containedBounds
				)) {
					consideredWidgets.set(
						contained,
						containedBounds
					);
				}
			}
			this.selectionWidget.visible= true;
			this.selectionWidget.width= 0;
			this.selectionWidget.height= 0;
			this.selectionWidget.transform= null;
			// this.selectionWidget.queue_allocate();
			this.boundDragData= new BoundDragData();
			this.boundDragData.consideredWidgets= consideredWidgets;
			this.boundDragData.selectionBounds= Graphene.Rect();
			this.boundDragData.selectionBounds.init(
				( float) startX,
				( float) startY,
				0.0f,
				0.0f
			);
			this.boundDragData.currentBounded= new Gee.HashSet< Gtk.Widget>( HashFuncs.widgetHashFunction);
			this.boundDragData.initialSelection= new Gee.HashSet< Gtk.Widget>( HashFuncs.widgetHashFunction);
			this.boundDragData.initialSelection.add_all( this._canvas.selection);
			this.boundDragData.currentAlterationMode= WidgetWithSelection.getAlterationModeFromModifiers( this.selectionDragGesture.get_current_event_state());
			// this.get_root().compute_transform(
			// 	this._canvas,
			// 	out this.boundDragData.windowToCanvasMatrix
			// );
			this.compute_transform(
				this._canvas,
				out this.boundDragData.viewToCanvasMatrix
			);
			this.boundDragData.placeholderPoint= Graphene.Point();
			this.boundDragData.originX= ( float) startX;
			this.boundDragData.originY= ( float) startY;
			// this.getCurrentCanvasPos(
			// 	out this.boundDragData.originX,
			// 	out this.boundDragData.originY
			// );
			this.boundDragData.blankTransform= new Gsk.Transform();
			this.alterSelectionDragStyling();
			this.selectionDragModifierKeyController.set_propagation_phase( Gtk.PropagationPhase.TARGET);
		}
		// private void getCurrentCanvasPos(
		// 	out int currentX,
		// 	out int currentY
		// ) {
		// 	double originSurfaceX, originSurfaceY;
		// 	this.selectionDragGesture.get_device().get_surface_at_position(
		// 		out originSurfaceX,
		// 		out originSurfaceY
		// 	);
		// 	this.boundDragData.placeholderPoint.x= ( float) originSurfaceX;
		// 	this.boundDragData.placeholderPoint.y= ( float) originSurfaceY;
		// 	this.boundDragData.placeholderPoint= this.boundDragData.viewToCanvasMatrix.transform_point(
		// 		this.boundDragData.placeholderPoint
		// 	);
		// 	currentX= ( int) this.boundDragData.placeholderPoint.x;
		// 	currentY= ( int) this.boundDragData.placeholderPoint.y;
		// }
		// Update could possibly pre-light contained node-s
		private void selectionDragUpdate(
			double offsetX,
			double offsetY
		) {
			// int currentX, currentY;
			// this.getCurrentCanvasPos(
			// 	out currentX,
			// 	out currentY
			// );
			float currentX= this.boundDragData.originX+ ( ( float) offsetX);
			float currentY= this.boundDragData.originY+ ( ( float) offsetY);

			float translateX, translateY, sizeX, sizeY;
			if( currentX< this.boundDragData.originX) {
				translateX= currentX;
				sizeX= this.boundDragData.originX;
			} else {
				translateX= this.boundDragData.originX;
				sizeX= currentX;
			}
			if( currentY< this.boundDragData.originY) {
				translateY= currentY;
				sizeY= this.boundDragData.originY;
			} else {
				translateY= this.boundDragData.originY;
				sizeY= currentY;
			}
			sizeX= sizeX- translateX;
			sizeY= sizeY- translateY;
			// float translateXFloat= ( float) translateX;
			// float translateYFloat= ( float) translateY;
			this.boundDragData.placeholderPoint.x= translateX;
			this.boundDragData.placeholderPoint.y= translateY;
			this.selectionWidget.transform= this.boundDragData.blankTransform.translate( this.boundDragData.placeholderPoint);
			this.selectionWidget.width= ( int) sizeX;
			this.selectionWidget.height= ( int) sizeY;
			this.selectionWidget.allocate(
				this.selectionWidget.width,
				this.selectionWidget.height,
				-1,
				this.selectionWidget.transform
			);
			// this.selectionWidget.queue_allocate();
			// this.selectionWidget.queue_draw();
			// this.queue_resize();
			// this.queue_draw();
			// this.layout_manager.layout_changed();
			this.boundDragData.selectionBounds.origin.x= translateX;
			this.boundDragData.selectionBounds.origin.y= translateY;
			this.boundDragData.selectionBounds.size.width= sizeX;
			this.boundDragData.selectionBounds.size.height= sizeY;
			this.boundDragData.selectionBounds= this.boundDragData.viewToCanvasMatrix.transform_bounds( this.boundDragData.selectionBounds);
			

			// Find those that are newly contained in the selection bound-s
			// Find those that are newly not contained in the selection bound-s

			Gtk.Widget contained;
			Gee.HashSet< Gtk.Widget> newBounded= new Gee.HashSet< Gtk.Widget>( HashFuncs.widgetHashFunction);
			bool boundAltered= false;
			foreach( Gee.Map.Entry< Gtk.Widget, Graphene.Rect?> entry in this.boundDragData.consideredWidgets.entries) {
				contained= entry.key; // locator
				if( this.selectionInfluenceFunction( this.boundDragData.selectionBounds, entry.value)) {
					newBounded.add( contained);
					if( !this.boundDragData.currentBounded.contains( contained)) {
						boundAltered= true;
						contained.add_css_class( "inSelectionBound");
					}
				} else {
					if( this.boundDragData.currentBounded.contains( contained)) {
						boundAltered= true;
						contained.remove_css_class( "inSelectionBound");
					}
				}
			}
			this.boundDragData.currentBounded= newBounded;
			if( boundAltered) {
				if( this.calculateResultantSelectionDuringDrag) {
					this.alterSelectionDragStyling();
				}
			}
			// this.queue_allocate();
			// this._canvas.queue_draw();
		}
		private void alterSelectionDragStyling() {
			foreach( Gtk.Widget widget in this.boundDragData.consideredWidgets.keys) {
				widget.remove_css_class( "removalCandidate");
				widget.remove_css_class( "additionCandidate");
			}
			Gee.HashSet< Gtk.Widget> currentBounded= new Gee.HashSet< Gtk.Widget>( HashFuncs.widgetHashFunction);
			currentBounded.add_all( this.boundDragData.currentBounded);
			Gee.HashSet< Gtk.Widget>? newlySelected, newlyUnSelected;
			bool altered= this._canvas.getNewSelectionStatus(
				currentBounded,
				this.boundDragData.currentAlterationMode,
				out newlySelected,
				out newlyUnSelected
			);
			if( altered) {
				foreach( Gtk.Widget widget in newlySelected) {
					widget.add_css_class( "additionCandidate");
				}
				foreach( Gtk.Widget widget in newlyUnSelected) {
					widget.add_css_class( "removalCandidate");
				}
			}
			
		}
		private void selectionDragEnd(
			double offset_x,
			double offset_y
		) {
			this.selectionWidget.visible= false;
			this.selectionDragModifierKeyController.set_propagation_phase( Gtk.PropagationPhase.NONE);
			foreach( Gtk.Widget contained in this.boundDragData.consideredWidgets.keys) {
				contained.remove_css_class( "additionCandidate");
				contained.remove_css_class( "removalCandidate");
			}
			foreach( Gtk.Widget widget in this.boundDragData.currentBounded) {
				widget.remove_css_class( "inSelectionBound");
			}
			this._canvas.alterSelectionWithWidgets(
				this.boundDragData.currentBounded,
				this.boundDragData.currentAlterationMode
			);
			this._canvas.raiseWidgets(
				this.boundDragData.currentBounded,
				false
			);
			this.boundDragData= null;
			this.queue_draw();
		}

		private bool selectionDragModifierPressed(
			Gtk.EventControllerKey controller,
			Gdk.ModifierType modifierState
		) {
			WidgetWithSelection.WidgetSelectionAlterationMode nextAlterationMode= WidgetWithSelection.getAlterationModeFromModifiers( this.selectionDragModifierKeyController.get_current_event_device().get_modifier_state());
			if( nextAlterationMode!= this.boundDragData.currentAlterationMode) {
				this.boundDragData.currentAlterationMode= nextAlterationMode;
				this.alterSelectionDragStyling();
			}
			return true; // Perhaps key-s can always be handle-ed during a drag
		}

		public override Gtk.SizeRequestMode get_request_mode() {
			return Gtk.SizeRequestMode.CONSTANT_SIZE;
		}
		public override void measure(
			Gtk.Orientation orientation,
			int for_size,
			out int minimum,
			out int natural,
			out int minimum_baseline,
			out int natural_baseline
		) {
			minimum= 1;
			natural= 1;
			minimum_baseline= -1;
			natural_baseline= -1;
		}
		public override void size_allocate(
			int width,
			int height,
			int baseline
		) {
			// if( this.boundDragData!= null) {
			// 	this.selectionWidget.allocate(
			// 		this.selectionWidget.width,
			// 		this.selectionWidget.height,
			// 		-1,
			// 		this.selectionWidget.transform
			// 	);
			// }
			this._canvas.allocate(
				1,
				1,
				// width,
				// height,
				-1,
				this._transformation
			);

		}
		public override void snapshot(
			Gtk.Snapshot snapshot
		) {
			if( this._canvas!= null) {
				this.snapshot_child(
					this._canvas,
					snapshot
				);
			}
			if( this.boundDragData!= null) {
				this.snapshot_child(
					this.selectionWidget,
					snapshot
				);
			}
		}
	}

	public struct PathConfig {
		public Gsk.Stroke stroke;
	}
	public struct ConnectionLayoutArgs {
		// it says that dot expect-s non negative integer-(.), unsure about other-(.) or if dot supports decimal specify-tion
		float? weight;
		/**
		* dot specific
		*/
		bool? constraint;
		// read doc-(.) for this one ↓
		int? minlen;
		/**
		* neato, fdp specific
		*/
		float? optimalLength;
		public ConnectionLayoutArgs.default() {
			weight= null;
			constraint= null;
			minlen= null;
			optimalLength= null;
		}
	}
	public enum ConnectionZLocation {
		OVER,
		UNDER
	}
	// TODO Can implement newly generic resize-able interface
	// ( public?) transformation abstract in interface
	/**
	* The widget that renders the connect-tion as well as handle-s function-al-ity
	*/
	public class Connection: Gtk.Widget {
		class construct {
			set_css_name( "Connection");
		}
		public static uint hash(
			Connection connection
		) {
			return uint.from_pointer( connection);
		}
		
		public bool oneEndDragged { get; set; default= false;}
		internal bool? _retainRejected= null;
		public bool? retainRejected {
			get {
				return this._retainRejected;
			}
			set {
				if( this._retainRejected== null) {
					throw new ConnectionRejection.NOT_REJECTION_CANDIDATE( "");
				} else {
					if( value== null) {
						throw new ConnectionRejection.NON_BOOL_VALUE( "");
					}
				}
				this._retainRejected= value;
			}
		}

		internal ConnectionEnd? _end0= null;
		public ConnectionEnd? end0 {
			get {
				return _end0;
			}
		}
		internal ConnectionEnd? _end1= null;
		public ConnectionEnd? end1 {
			get {
				return _end1;
			}
		}
		internal Gsk.Path path;
		internal PathConfig pathConfig;
		private Graphene.Matrix? thisToCanvasTransform= null;
		// internal Position end0Position;
		// internal Position end1Position;
		// TODO line style preference
		// Maybe Gsk.Stroke,
		//   Also need line shape info


		public ConnectionLayoutArgs layoutArgs { get; set; default= ConnectionLayoutArgs.default();}



		public Connection() {
			this.path= Gsk.Path.parse( "");
			this.pathConfig= PathConfig() {
				stroke= new Gsk.Stroke( 4.0f)
			};
			// this.pathConfig.stroke.set_dash()
		}
		public void removeEnd(
			ConnectionEnd end
		) {
			if( end== this._end0) {
				this._end0= null;
			} else if( end== this._end1) {
				this._end1= null;
			}
		}
		public ConnectionEnd? getOtherEnd(
			ConnectionEnd end
		) {
			if( end== this._end0) {
				return this._end1;
			} else if( end== this._end1) {
				return this._end0;
			} else {
				return null;
			}
		}
		public override bool contains(
			double x,
			double y
		) {
			if( this.thisToCanvasTransform== null) {
				return false;
			}
			float distance;
			Graphene.Point point= Graphene.Point();
			point.init(
				( float) x,
				( float) y
			);
			point= this.thisToCanvasTransform.transform_point(
				point
			);
			Gsk.PathPoint pathPoint;
			bool found= this.path.get_closest_point(
				point,
				this.pathConfig.stroke.get_line_width()* 4.0f,
				// 150.0f,
				out pathPoint,
				out distance
			);
			return found;
			// print( "%s, %f, %f\n", found.to_string(), distance, this.pathConfig.stroke.get_line_width());
			if( distance< ( this.pathConfig.stroke.get_line_width()/ 2.0f)) {
				return true;
			} else {
				return false;
			}
		}
		internal void recreatePath() {
			Graphene.Point? start;
			Graphene.Point? end;
			if( this.oneEndDragged) {
				ConnectionEnd present;
				if( this.end0!= null) {
					present= this.end0;
				} else {
					present= this.end1;
				}
				start= present.ownPositionCanvas;
				end= present.targetNode.canvas.connectionEndDragData.cursorPosition;
			} else {
				start= this.end0.ownPositionCanvas;
				end= this.end1.ownPositionCanvas;
				// thisToCanvasTransform only used when both ends connect-ed so should be ok to just use one end
				if( this.end0.targetNode== null) {
					this.thisToCanvasTransform= null;
				} else {
					bool transformComputeSucses= this.compute_transform(
						this.end0.targetNode.canvas,
						out this.thisToCanvasTransform
					);
					if( !transformComputeSucses) {
						this.thisToCanvasTransform= null;
					}
				}
			}
			
			Gsk.PathBuilder pathBuilder= new Gsk.PathBuilder();
			pathBuilder.move_to(
				start.x,
				start.y
			);
			pathBuilder.line_to(
				end.x,
				end.y
			);
			this.path= pathBuilder.to_path();
		}

	}

	public enum ConnectionEndDragType{
		DRAG_AWAY,
		CREATE
	}
	/**
	* Implement-ed to allow a widget to act as the site of connect-tion for a connect-tion on some canvas
	*/
	public interface ConnectionEnd: Gtk.Widget {
		public static uint hash(
			ConnectionEnd connectionEnd
		) {
			return uint.from_pointer( connectionEnd);
		}

		private abstract Gdk.ModifierType dragAwayModifierPrivate { get; set; default= Gdk.ModifierType.SHIFT_MASK;}
		public Gdk.ModifierType dragAwayModifier {
			get {
				return this.dragAwayModifierPrivate;
			}
			set {
				this.ensureUniqueDragModifiers(
					value,
					this.createModifierPrivate
				);
				this.dragAwayModifierPrivate= value;
			}
		}
		private abstract Gdk.ModifierType createModifierPrivate { get; set; default= Gdk.ModifierType.NO_MODIFIER_MASK;}
		public Gdk.ModifierType createModifier {
			get {
				return this.createModifierPrivate;
			}
			set {
				this.ensureUniqueDragModifiers(
					this.dragAwayModifierPrivate,
					value
				);
				this.createModifierPrivate= value;
			}
		}
		public abstract bool canDragCreateConnections { get; set; default= true;}
		public abstract bool canDragConnectionsAway { get; set; default= true;}
		public abstract Gtk.GestureDrag dragController { get; set;}
		public abstract bool connectionTargetHover { get; set; default= false;}
		// public abstract Gtk.EventControllerMotion motionController { get; set;}
		public abstract ConnectionZLocation zLocation { get; set; default= ConnectionZLocation.OVER;}
		/**
		* In connection end space
		*/
		protected abstract Graphene.Point connectionSource { get;}
		/**
		* In canvas space
		*/
		public abstract Graphene.Point connectionOffset { get; set;}
		// TODO NodeWrapper must point to canvas
		// Then DragArea can also implement this behaviour
		private abstract NodeWrapper? targetNodePrivate { get; set; default= null;}
		public NodeWrapper? targetNode {
			get {
				if( this.targetNodePrivate== null) {
					Gtk.Widget? currentWidget= ( Gtk.Widget) this;
					while( true) {
						if( currentWidget is NodeWrapper) {
							break;
						}
						currentWidget= currentWidget.get_parent();
						if( currentWidget== null) {
							break;
						}
					}
					if( currentWidget!= null) {
						this.targetNodePrivate= ( NodeWrapper) currentWidget;
						this.registerWithTargetNode();
					}
				}
				return this.targetNodePrivate;
			}
			set {
				if( this.targetNodePrivate!= null) {
					if( this.targetNodePrivate== value) {
						return;
					}
					this.unregisterWithTargetNode();
				}
				this.targetNodePrivate= value;
			}
		}
		private abstract Graphene.Point? connectionOffsetCanvasPrivate { get; set; default= null;}
		internal Graphene.Point? connectionOffsetCanvas {
			get {
				if( this.connectionOffsetCanvasPrivate== null) {
					bool sucses= this.getNewPositionInformation();
					if( !sucses) {
						return null;
					}
				}
				return this.connectionOffsetCanvasPrivate;
			}
		}
		private abstract Graphene.Point? ownPositionCanvasPrivate { get; set; default= null;}
		public Graphene.Point? ownPositionCanvas {
			get {
				if( this.ownPositionCanvasPrivate== null) {
					// print( "GOPC RECALCULATE: ");
					bool sucses= this.getNewPositionInformation();
					// print( sGOPC ucses.to_string());
					// print( "GOPC \n");
					if( !sucses) {
						return null;
					}
				}
				return this.ownPositionCanvasPrivate;
			}
		}
		private abstract Graphene.Matrix? thisToCanvasTransformPrivate { get; set; default= null;}
		internal Graphene.Matrix? thisToCanvasTransform {
			get {
				if( this.thisToCanvasTransformPrivate== null) {
					bool sucses= this.getNewPositionInformation();
					if( !sucses) {
						return null;
					}
				}
				return this.thisToCanvasTransformPrivate;
			}
		}
		private bool getNewPositionInformation() {
			if( this.targetNode!= null) {
				Graphene.Matrix transform;
				bool sucses= this.compute_transform(
					this.targetNode.canvas,
					out transform
				);
				if( sucses) {
					this.ownPositionCanvasPrivate= transform.transform_point(
						this.connectionSource
					);
					this.connectionOffsetCanvasPrivate= Graphene.Point().init(
						this.ownPositionCanvasPrivate.x+ this.connectionOffset.x,
						this.ownPositionCanvasPrivate.y+ this.connectionOffset.y
					);
					this.thisToCanvasTransformPrivate= transform;
					return true;
				}
			}
			this.ownPositionCanvasPrivate= null;
			this.connectionOffsetCanvasPrivate= null;
			this.thisToCanvasTransformPrivate= null;
			return false;
		}
		internal abstract Gee.HashSet< Connection> connectionsInternal { get; set;}
		public Gee.HashSet< Connection> connections {
			get {
				return this.connectionsInternal;
			}
		}
		protected void ConnectionEndSetup() {
			this.connectionsInternal= new Gee.HashSet< Connection>(
				Connection.hash
			);
			this.connectionOffset= Graphene.Point();
			this.dragController= new Gtk.GestureDrag();
			this.dragController.drag_begin.connect( this.dragBegin);
			this.dragController.drag_update.connect( this.dragUpdate);
			this.dragController.drag_end.connect( this.dragEnd);
			this.add_controller( this.dragController);
			// this.motionController= new Gtk.EventControllerMotion();
			// this.motionController.set_propagation_phase( Gtk.PropagationPhase.CAPTURE);
			// this.motionController.set_propagation_limit( Gtk.PropagationLimit.NONE);
			// this.motionController.enter.connect( this.motionEnter);
			// this.motionController.leave.connect( this.motionLeave);
			// this.add_controller( this.motionController);
		}
		// internal void onConnectionTargetHover() {
		// 	print( "CONNECTION TARGET HOVER\n");
		// 	this.add_css_class( "connectionTargetHover");
		// 	this.targetNode.canvas.connectionEndDragData.hoveredTarget= this;
		// 	this.connectionTargetHover= true;
		// 	bool accepted;
		// 	bool passOccured= false;
		// 	bool rejectionOccured= false;
		// 	bool selfConnectProposalOccured= false;
		// 	string acceptanceClass;
		// 	ConnectionEnd existingConnectedEnd;
		// 	foreach( Connection connection in this.targetNode.canvas.connectionEndDragData.connections) {
		// 		connection.add_css_class( "connectionTargetHover");
		// 		if( connection.end0!= null) {
		// 			existingConnectedEnd= connection.end0;
		// 		} else {
		// 			existingConnectedEnd= connection.end1;
		// 		}
		// 		if ( existingConnectedEnd== this) {
		// 			acceptanceClass= "rejected";
		// 			connection.add_css_class( "selfConnectProposed");
		// 			accepted= false;
		// 			this.targetNode.canvas.connectionEndDragData.selfConnectProposedRejection.add( connection);
		// 			if( !selfConnectProposalOccured) {
		// 				selfConnectProposalOccured= true;
		// 				this.add_css_class( "selfConnectProposed");
		// 			}
		// 		} else {
		// 			accepted= this.acceptsConnection(
		// 				InteractionSource.GUI,
		// 				this.targetNode.canvas.connectionEndDragData.startConnectionEnd,
		// 				connection
		// 			);
		// 			if( accepted) {
		// 				acceptanceClass= "accepted";
		// 				this.targetNode.canvas.connectionEndDragData.acceptedConnections.add( connection);
		// 			} else {
		// 				acceptanceClass= "rejected";
		// 				this.targetNode.canvas.connectionEndDragData.nonAcceptanceRejection.add( connection);
		// 			}
		// 		}
		// 		connection.add_css_class( acceptanceClass);
		// 		passOccured= passOccured|| accepted;
		// 		rejectionOccured= rejectionOccured|| ( !accepted);
		// 	}
		// 	if( passOccured&& rejectionOccured) {
		// 		acceptanceClass= "partialAccept";
		// 	} else if( passOccured) {
		// 		acceptanceClass= "fullAccept";
		// 	} else { // if( rejectionOccured)
		// 		acceptanceClass= "noAccept";
		// 	}
		// 	this.add_css_class( acceptanceClass);
		// }
		// internal void onConnectionTargetExit() {
		// 	print( "CONNECTION TARGET EXIT\n");
		// 	this.remove_css_class( "connectionTargetHover");
		// 	foreach( Connection connection in this.targetNode.canvas.connectionEndDragData.connections) {
		// 		connection.remove_css_class( "connectionTargetHover");
		// 		connection.remove_css_class( "accepted");
		// 		connection.remove_css_class( "rejected");
		// 		connection.remove_css_class( "selfConnectProposed");
		// 	}
		// 	this.remove_css_class( "partialAccept");
		// 	this.remove_css_class( "fullAccept");
		// 	this.remove_css_class( "noAccept");
		// 	this.remove_css_class( "selfConnectProposed");
		// 	this.connectionTargetHover= false;
		// }

		// private void motionEnter(
		// 	double x,
		// 	double y
		// ) {
		// 	print( "ENTER REGISTERED\n");
		// 	if( this.targetNode== null) {
		// 		print( "q\n");
		// 		return;
		// 	}
		// 	if( this.targetNode.canvas.connectionEndDragData== null) {
		// 		print( "w\n");
		// 		return;
		// 	}
		// 	this.onConnectionTargetHover();
		// }
		// private void motionLeave() {
		// 	if( this.connectionTargetHover) {
		// 		this.onConnectionTargetExit();
		// 		this.targetNode.canvas.connectionEndDragData.hoveredTarget= null;
		// 		this.targetNode.canvas.connectionEndDragData.acceptedConnections.clear();
		// 		this.targetNode.canvas.connectionEndDragData.selfConnectProposedRejection.clear();
		// 		this.targetNode.canvas.connectionEndDragData.nonAcceptanceRejection.clear();
		// 	}
		// }

		private void ensureUniqueDragModifiers(
			Gdk.ModifierType disconnect,
			Gdk.ModifierType create
		) throws ModifierClass {
			if ( disconnect== create) {
				throw new ModifierClass.NON_UNIQUE_MODIFIERS(
					""
				);
			}
		}
		private void dragBegin(
			double startX,
			double startY
		) {
			Gdk.ModifierType modifier= this.dragController.get_current_event_state();
			if ( this.targetNode== null) {
				this.dragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			ConnectionEndDragType dragType;
			if ( modifier== this.createModifierPrivate) {
				dragType= ConnectionEndDragType.CREATE;
			} else if ( modifier== this.dragAwayModifierPrivate) {
				dragType= ConnectionEndDragType.DRAG_AWAY;
			} else {
				this.dragController.set_state( Gtk.EventSequenceState.DENIED);
				return;
			}
			this.targetNode.canvas.connectionEndDragStart(
				this,
				dragType,
				this.dragController,
				startX,
				startY
			);

		}
		private void dragUpdate(
			double offsetX,
			double offsetY
		) {

			this.targetNode.canvas.connectionEndDragUpdate(
				this,
				this.dragController,
				offsetX,
				offsetY
			);
		}
		private void dragEnd(
			double offsetX,
			double offsetY
		) {
			this.targetNode.canvas.connectionEndDragEnd(
				this,
				this.dragController,
				offsetX,
				offsetY
			);
		}
		internal void invalidateThisToCanvasTransform() {
			this.thisToCanvasTransformPrivate= null;
			this.ownPositionCanvasPrivate= null;
			this.connectionOffsetCanvasPrivate= null;
		}
		/**
		* Test for acceptance of a new connection
		*/
		public virtual bool acceptsConnection(
			InteractionSource interactionSource,
			ConnectionEnd otherEnd,
			Connection connection
		) {
			return true;
		}
		private void registerWithTargetNode() {
			this.targetNodePrivate.connectionEnds.add( this);
		}
		/**
		* Call upon removal of `this` from the node hierarchy of the node to which it targets
		*/
		public bool unregisterWithTargetNode() {
			if( this.targetNodePrivate!= null) {
				return this.targetNodePrivate.connectionEnds.remove( this);
			} else {
				return false;
			}
		}

		// public void connectionEndCallOnSizeALlocate(
		// 	int width,
		// 	int height,
		// 	int baseline
		// ) {
		// 	print( "HIII\n");
		// }
		// public abstract void size_allocate(
		// 	int width,
		// 	int height,
		// 	int baseline
		// );

		// TODO dragging controller-s and it-s~s function-s
	}

	/**
	* An implement-tion of a connect-tion end as a stand alone widget
	*/
	public class ConnectionEndStub: Gtk.Widget, ConnectionEnd {
		private Gdk.ModifierType dragAwayModifierPrivate { get; set; default= Gdk.ModifierType.SHIFT_MASK;}
		private Gdk.ModifierType createModifierPrivate { get; set; default= Gdk.ModifierType.NO_MODIFIER_MASK;}
		protected Graphene.Point connectionSource {
			get {
				Graphene.Point point= Graphene.Point();
				point.init(
					( ( float) size.width)/ 2,
					( ( float) size.height)/ 2
				);
				return point;
			}
		}
		public bool canDragCreateConnections { get; set; default= true;}
		public bool canDragConnectionsAway { get; set; default= true;}
		public Gtk.GestureDrag dragController { get; set;}
		public bool connectionTargetHover { get; set; default= false;}
		// public Gtk.EventControllerMotion motionController { get; set;}
		public ConnectionZLocation zLocation { get; set; default= ConnectionZLocation.OVER;}
		private Graphene.Point? connectionOffsetCanvasPrivate { get; set; default= null;}
		private Graphene.Point? ownPositionCanvasPrivate { get; set; default= null;}
		public Graphene.Point connectionOffset { get; set;}
		private Graphene.Matrix? thisToCanvasTransformPrivate { get; set; default= null;}
		private NodeWrapper? targetNodePrivate { get; set; default= null;}
		internal Gee.HashSet< Connection> connectionsInternal { get; set;}
		public Gtk.Requisition size= Gtk.Requisition() {
			width= 15,
			height= 15
		};
		class construct {
			set_css_name( "ConnectionEndStub");
		}
		public ConnectionEndStub() {
			this.ConnectionEndSetup();
		}
		public override Gtk.SizeRequestMode get_request_mode() {
			return Gtk.SizeRequestMode.CONSTANT_SIZE;
		}
		public override void measure(
			Gtk.Orientation orientation,
			int forSize,
			out int minimum,
			out int natural,
			out int minimumBaseline,
			out int naturalBaseline
		) {
			switch( orientation) {
				case( Gtk.Orientation.HORIZONTAL):
					minimum= natural= this.size.width;
					break;
				case( Gtk.Orientation.VERTICAL):
					minimum= natural= this.size.height;
					break;
			}
			minimumBaseline= naturalBaseline= -1;
		}
		// public override void size_allocate(
		// 	int width,
		// 	int height,
		// 	int baseline
		// ) {
		// 	print( "SALLOC\n");
		// 	this.connectionEndCallOnSizeALlocate( width, height, baseline);
		// }
	}
}

class CanvasViewTempOptions {
	int? panButton= 2;
	bool zoomAllowed= true;
}

/*
Other change-s:
	CanvasView:
		- ( min, max) scale
		- maybe:
			- zoom to fit all    \
			                      ----- - frame all
			- move to all center /
			- zoom to fit selection    \
			                      ----- - frame selection
			- move to selection center /
		- maybe shortcut view like in helix


Connection z order
Canvas can have connection-s under or over
Connection end-s can have ( under, over) preference
If the end matches the canvas preference then nothing needs doing
otherwise:
	canvas under, node over:
		- could draw the connection within widget bonud-s whilst drawing widget
	canvas over, node under:
		- could draw widget whilst drawing connection
		- could mask the widget whilst drawing connection
*/
